<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Monitoring Words :: by :: JAWEB.ma</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/metro.css" rel="stylesheet" />
    <link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/style_responsive.css" rel="stylesheet" />
    <link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
    <link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
    <?php
        session_start();
        if(isset($_SESSION['uname'])){
            $uname = $_SESSION['uname'];
            $upass = $_SESSION['upass'];
        }
        if(empty($uname) || empty($upass)){
            //echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
            die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
        }
        $inpage = 'search';
       
    ?>
    <!-- BEGIN HEADER -->
    <div class="header navbar navbar-inverse">
        <!-- BEGIN TOP NAVIGATION BAR -->
        <div class="">
            <div class="container-fluid">
                <!-- BEGIN LOGO -->
                <a class="brand" href="index.php">
                <img src="assets/img/logo.png" alt="logo" />
                </a>
                <!-- END LOGO -->
            </div>
        </div>
        <!-- END TOP NAVIGATION BAR -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->    
    <div class="page-container row-fluid" style="margin-top:-50px;">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar nav-collapse collapse">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <div class="slide hide">
                <i class="icon-angle-left"></i>
            </div>

<br /><br />

            <div class="clearfix"></div>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
            <!-- BEGIN SIDEBAR MENU -->
<?php
    include('sidebar.php');
?>
            <!-- END SIDEBAR MENU -->


        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">

            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN STYLE CUSTOMIZER-->
                        
                        <!-- END STYLE CUSTOMIZER-->  
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        
                        <h3 class="page-title">
                            Rechercher
                            <small>
                                
                                
                                Résultats de recherche
                            
                            </small>
                        </h3>
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">Tableau de bord</a> 
                            </li>
                            <span class="icon-angle-right"></span>
                            <li>
                                <a href="search.php?section=search">Rechercher</a> 
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
                        


                        <!-- BEGIN DASHBOARD STATS -->
                    <div class="row-fluid">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <h4><i class="icon-reorder"></i>
                                    
                                        Résultats de recherche

                                
                                </h4>
                             </div>

                             <div class="portlet-body form">


                                  

                                            <table class="table table-striped table-bordered">
                                                <tr>
                                                    <th width="200" style="text-align:center;">Titre</th>
                                                    <th width="60" style="text-align:center;">Lien</th>
                                                    <th width="500" style="text-align:center;">Description</th>
                                                    <th width="180" style="text-align:center;">Enregistrement</th>
                                                </tr>
                                               
                                                <?php include('config.php');

                                                function sel_option($val1, $val2){
                                            if ($val1 == $val2){
                                                return "selected";
                                            }else{
                                                return "";
                                            }
                                        }


$archivealert= mysql_query('SELECT * FROM `archivealert`');
?>
                                         
<?php  while ($row=mysql_fetch_assoc($archivealert)) { ?>
                                        <form action="codes.php" method="GET">
                                         <input name="cron" value="cronArchive" type="hidden">
                                                <tr>
                                                    <td><?php echo $row['titrearticle']?></td>
                                                    <td style="text-align:center;"><a class="btn blue" href="<?php echo $row['url']?>" target="_blank"><i class="icon-link"></i></a></td>
                                                    <td><?php echo $row['description']?></td>
                                                    <td>
                                                    <div class="control-group form-inline">
                                                      <div class="controls">
                                                         <select tabindex="1" name="sid" data-placeholder="Sélectionner un secteur" class="span6 m-wrap">
                                                            <option value="">Sélectionner un secteur...</option>

                                                            <?php
                                                                $scc = mysql_fetch_assoc(mysql_query('select * from archive where url="'.$row['url'].'"'));
                                                                $secteurs = mysql_query('select * from secteur');
                                                                if(mysql_num_rows($secteurs) == 0){
                                                                    echo'<option value="">Aucun secteur trouvé</option>';
                                                                }else{
                                                                    while ($se = mysql_fetch_assoc($secteurs)) {
                                                                        echo '<option value="'.$se['id'].'" '.sel_option($se['id'],$scc['linkid']).'>'.utf8_encode($se['name']).'</option>';
                                                                    }
                                                                }
                                                           ?>
                                                        </select>
                                                        <select tabindex="1" name="pid" data-placeholder="Sélectionner un préfecture" class="span6 m-wrap">
                                                            <option value="">Sélectionner un préfecture...</option>

                                                            <?php
                                                                $prefecteurs = mysql_query('select * from prefecteur');
                                                                if(mysql_num_rows($prefecteurs) == 0){
                                                                    echo'<option value="">Aucun prefecteur trouvé</option>';
                                                                }else{
                                                                    while ($prefecteur = mysql_fetch_assoc($prefecteurs)) {
                                                                        echo '<option value="'.$prefecteur['id'].'" '.sel_option($prefecteur['id'],$scc['prefecteur_id']).'>'.utf8_encode($prefecteur['name']).'</option>';
                                                                    }
                                                                }
                                                           ?>
                                                        </select>
                                                        <?php
                                                        if($row['url']== $scc['url']){
                                                            echo '<span class="btn green">Enregistré</span>';
                                                        }else{
                                                            echo '<button type="submit" class="btn blue btn-success">Enregistrer</button>';
                                                        }
                                                        ?>
                                                      </div>
                                                    </div>
                                                        
                                                        <input name="titre" value="<?php echo $row['titrearticle']; ?>" type="hidden">
                                                        <input name="url" value="<?php echo $row['url']; ?>" type="hidden">
                                                        <input name="desc" value="<?php echo $row['description']; ?>" type="hidden">
                                                        <input name="key" value="<?php echo $key; ?>" type="hidden">
                                                        <input name="siteweb" value="<?php echo $siteweb; ?>" type="hidden">
                                                    </form>
                                                    </td>
                                                    </tr>
                                        <?php } ?>
                                                
                                                 </table>
                                                 

                                           
                             </div>
                            
                        </div>
                           
                    </div>

                    
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->  
        </div>
        <!-- END PAGE -->       
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="footer">
        Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
        <br />
        &copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
        <div class="span pull-right">
            <span class="go-top"><i class="icon-angle-up"></i></span>
        </div>
    </div>
    <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->
    <!-- Load javascripts at bottom, this will reduce page load time -->
    <script src="assets/js/jquery-1.8.3.min.js"></script>           
    <script src="assets/breakpoints/breakpoints.js"></script>           
    <script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>  
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.blockui.js"></script>
    <script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>    
    <script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- ie8 fixes -->
    <!--[if lt IE 9]>
    <script src="assets/js/excanvas.js"></script>
    <script src="assets/js/respond.js"></script>
    <![endif]-->
    <script src="assets/js/app.js"></script>        
    <script>
        jQuery(document).ready(function() {         
            // initiate layout and plugins
            App.setPage('calendar');
            App.init();
        });
    </script>
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-37564768-1']);
      _gaq.push(['_setDomainName', 'keenthemes.com']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>