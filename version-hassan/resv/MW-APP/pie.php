<html>
  <head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Secteur');
        data.addColumn('number', 'N.article');
        data.addRows([
          <?php
              $secteur = mysql_query('select * from secteur');
              while ($secteur_name = mysql_fetch_assoc($secteur)) {
                $nbarticle = mysql_fetch_assoc(mysql_query('select count(*) as nbarticle from archive where sid="'.$secteur_name['id'].'"'));
                echo "['".addslashes(utf8_encode($secteur_name['name']))."', ".$nbarticle['nbarticle']."],
                ";
              }
          ?>
        ]);

        // Set chart options
        var options = {'title':'Nombre des articles pour chaque secteur',
                       'width':600,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div align="center" id="chart_div"></div>
  </body>
</html>