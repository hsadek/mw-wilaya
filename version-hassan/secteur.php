<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'secteur';
		$sect = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							Secteurs
							<small>
								<?php
			                     	if($section=='add'){ echo "Ajouter un secteur";}
			                     	if($section=='list'){ echo "Gérer la liste des secteurs";}
			                     	if($section=='remove'){ echo "Supprimer un secteur";}
			                   		if($section=='update'){ echo "Mise à jour un secteur";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.php">Tableau de bord</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="secteur.php?section=list">Secteurs</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->

					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						


						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
							<?php
								if (isset($_GET["msg"]))		
								$msg = htmlspecialchars($_GET["msg"]);
								if($section == 'list' && $msg == 'ok'){
									$alert = 'success';
									$msgs = 'Votre secteur a été bien ajouté.';
								}elseif($section == 'list' && $msg == 'del'){
									$alert = 'success';
									$msgs = 'Votre secteur a été bien supprimé.';
								}elseif($section == 'list' && $msg == 'update'){
									$alert = 'success';
									$msgs = 'Votre secteur a été bien modifié.';
								}
							?>
							<?php
								if (isset($msg)){
							?>
								<div class="alert alert-<?php echo $alert; ?> alert-dismissable">
								  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  	<center><?php echo $msgs; ?></center>
								</div>
							<?php
								}
							?>
						<div class="portlet box blue">

							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
			                     	<?php
			                     		if($section=='add'){ echo "Ajouter un secteur";}
			                     		if($section=='list'){ echo "Liste des secteurs";}
			                     		if($section=='remove'){ echo "Supprimer un secteur";}
			                     		if($section=='update'){ echo "Mise à jour un secteur";}
			                     	?>
		                        </h4>
		                    </div>

		                     <div class="portlet-body form">
		                     	<?php
		                     		if($section=='add'){
		                     	?>
		                     	<form class="form-horizontal" action="codes.php" method="GET">
		                     		<input name="controller" value="addsecteur" type="hidden">

			                     	<div class="control-group">
		                              <label class="control-label">Nom de secteur</label>
		                              <div class="controls">
		                                 <input type="text" name="name" data-original-title="Inserer le nom de secteur" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>

		                           	<div class="form-actions">
		                              <button class="btn blue" type="submit">Enregistrer</button>
		                           </div>

		                        </form>   
		                     	<?php
		                     		} // End of ADD
		                     	?>

		                     	<?php
		                     	include('config.php');
		                     		if($section=='list'){
										$page = (!isset($_GET['p']) ? 1 : intval($_GET['p']));
										$page =(intval($_GET['p'])<=0 ? 1 : $page );
										$pnum = 10;
										$limit = ($page * $pnum) - $pnum;
		                     			$secteurs = mysql_query('select * from secteur limit '.$limit.','.$pnum.'');
		            					if(mysql_num_rows($secteurs) == 0){
		            						echo 'aucun secteur trouvé';
		            					}else{
		            						echo '
		            						<table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">#</th>
			            							<th style="text-align:center;">Nom</th>
			            							<th style="text-align:center;">Nombre d\'article</th>
			            							<th style="text-align:center;">Action</th>
			            						</tr>
		            						';
		            						$i = (($page * $pnum)-$pnum)+1;
		            						while ($s = mysql_fetch_assoc($secteurs)) {
		            							$nbarticle = mysql_fetch_assoc(mysql_query('select count(*) as nbarticle from archive where sid="'.$s['id'].'"'));
		            							echo '
												<tr>
			            							<td>'.$i.'</td>
			            							<td>'.utf8_encode($s['name']).'</td>
			            							<td>'.$nbarticle['nbarticle'].'</td>
			            							<td style="text-align:center;"><a class="btn red" OnClick="return confirm(\'Voulez vous vraiment supprimer cette secteur ?\')" href="secteur.php?section=remove&id='.$s['id'].'"><i class="icon-trash"></i></a> 
			            								<a class="btn blue" href="secteur.php?section=update&id='.$s['id'].'"><i class="icon-pencil"></i></a>
			            							</td>
			            						</tr>
		            							';
		            							$i++;
		            						}
		            						echo '</table>';
											$pagequery = mysql_fetch_assoc(mysql_query('select count(*) as pg from secteur'));
											$tp = $pagequery["pg"];
											$actual_link = '?section=list';
											$totalp = ceil($tp/$pnum);
											$totalpr = $totalp - 1;
											$prev = $page - 1;
											$next = $page + 1;
											echo '<div class="pagination pagination-centered">
													<ul>';
													if($page > 1){
														echo '<li><a href="'.$actual_link.'&p='.$prev.'">&laquo;</a></li>';
													}else{
														echo '<li class="disabled"><a href="#">&laquo;</a></li>';
													}
													
													if($totalp < 7 + ($pnum * 2)){
														for($i=1;$i<=$totalp;$i++){
														if($i == $page)
															echo '<li class="active"><a href="#">'.$i.'</a></li>';
														else
															echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
														}
													}elseif($totalp > 5 + ($pnum * 2)){
														if($page < 1 + ($pnum * 2)){
															for($i=1;$i<=4+($pnum * 2);$i++){
															if($i == $page)
																echo '<li class="active"><a href="#">'.$i.'</a></li>';
															else
																echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
															}
															echo '<li><a href="#">...</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalpr.'">'.$totalpr.'</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalp.'">'.$totalp.'</a></li>';
														}elseif($totalp - ($pnum * 2) > $page && $page > ($pnum * 2)){
															echo '<li><a href="'.$actual_link.'&p=1">1</a></li>';
															echo '<li><a href="'.$actual_link.'&p=2">2</a></li>';
															echo '<li><a href="#">...</a></li>';
															for($i=$page-$pnum;$i<=$page+$pnum;$i++){
															if($i == $page)
																echo '<li class="active"><a href="#">'.$i.'</a></li>';
															else
																echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
															}
															echo '<li><a href="#">...</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalpr.'">'.$totalpr.'</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalp.'">'.$totalp.'</a></li>';
														}else{
															echo '<li><a href="'.$actual_link.'&p=1">1</a></li>';
															echo '<li><a href="'.$actual_link.'&p=2">2</a></li>';
															echo '<li><a href="#">...</a></li>';
															for($i=$totalp-(2+($pnum * 2));$i<=$totalp;$i++){
															if($i == $page)
																echo '<li class="active"><a href="#">'.$i.'</a></li>';
															else
																echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
															}
														}
													}
													
													if($page < $totalp){
														echo '<li><a href="'.$actual_link.'&p='.$next.'">&raquo;</a></li>';
													}else{
														echo '<li class="disabled"><a href="#">&raquo;</a></li>';
													}
													echo'
													</ul>
												</div>';
			            				}
		                     		}
		                     	?>


		                     	<?php
		                     		if($section=='remove'){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if (isset($section) && isset($id)){
											if($section=='remove'){							
												mysql_query('delete from secteur where id="'.$id.'"');
												//header('location : secteur.php?section=list&msg=ok');
												echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; secteur.php?section=list&msg=del" /> ';
											}
										}
									}
		                     	?> 	

		                     	<?php
		                     		if($section=='update'){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if(isset($id)){
		                     				$sec = mysql_fetch_assoc(mysql_query('select * from secteur where id="'.$id.'"'));
		                     	?>
		                     	
		                     	<form class="form-horizontal" action="codes.php" method="GET">
		                     		<input name="controller" value="updatesecteur" type="hidden">
									<input name="id" value="<?php echo $sec['id']; ?>" type="hidden">
			                     	<div class="control-group">
		                              <label class="control-label">Nom de secteur </label>
		                              <div class="controls">
		                                 <input type="text" name="name" value="<?php echo $sec['name']; ?>" data-original-title="Inserer le nom de secteur" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>

		                           	<div class="form-actions">
		                              <button class="btn blue" type="submit">Enregistrer</button>
		                           </div>

		                        </form>   
		                     	<?php
		                     			}
		                     		} // End of ADD
		                     	?>


		                     </div>
		                <?php
		                }
		                else{
		                ?>
							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php	
		                }
		                ?>     


		                </div>
		                   
					</div>

					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>