g			<ul>
				<li <?php if($inpage == 'index') echo 'class="active"'; ?>>
					<a href="index.php">
					<i class="icon-home"></i> Tableau de bord
					</a>					
				</li>

				<li class="has-sub <?php if($inpage == 'compte') echo 'active'; ?>">
					<a href="compte.php?section=compte">
					<i class="icon-user"></i> Mon Compte
					</a>					
				</li>

				<!--<li><a class="" href="charts.html"><i class="icon-bar-chart"></i>Archives & statistiques</a></li>-->
				<li class="has-sub <?php if($inpage == 'search') echo 'active'; ?>">
					<a href="search.php?section=search">
					<i class="icon-globe"></i> Rechercher
					</a>					
				</li>
				<li class="has-sub <?php if($inpage == 'alerte') echo 'active'; ?>">
					<a href="javascript:;" class="">
					<i class="icon-bell-alt"></i>  Alerte
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub">
						<li <?php if($inpage == 'alerte' && $sect == 'list') echo 'class="active"'; ?>>
							<a class="" href="liste.php?section=liste">Liste
							</a>
						</li>
						<li <?php if($inpage == 'alerte' && $sect == 'cre') echo 'class="active"'; ?>><a class="" href="alerte.php?section=search">Création</a></li>
					</ul>
				</li>
				<li class="has-sub <?php if($inpage == 'presse') echo 'active'; ?>">
					<a href="javascript:;" class="">
					<i class="fa fa-list-alt"></i>  E-Presse
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub">
						<li <?php if($inpage == 'presse' && $sect == 'presse') echo 'class="active"'; ?>>
							<a class="" href="presse.php?section=presse">Ajouter revue de presse
							</a>
						</li>
						<li <?php if($inpage == 'presse' && $sect == 'list') echo 'class="active"'; ?>>
							<a class="" href="liste_titre.php?section=list">Liste revue de presse
							</a>
						</li>
						<li <?php if($inpage == 'presse' && $sect == 'des') echo 'class="active"'; ?>>
							<a class="" href="destinataires.php?section=des">Destinataires
							</a>
						</li>
						<li <?php if($inpage == 'presse' && $sect == 'logs') echo 'class="active"'; ?>>
							<a class="" href="historique_envoie.php?section=list">Historique d'envoie
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub <?php if($inpage == 'siteweb') echo 'active'; ?>">
					<a href="javascript:;" class="">
					<i class="icon-desktop"></i> Sites web
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub">
						<li <?php if($inpage == 'siteweb' && $sect == 'add') echo 'class="active"'; ?>>
							<a class="" href="siteweb.php?section=add">Ajouter
							</a>
						</li>
						<li <?php if($inpage == 'siteweb' && $sect == 'list') echo 'class="active"'; ?>><a class="" href="siteweb.php?section=list">Liste</a></li>
					</ul>
				</li>

				<li class="has-sub <?php if($inpage == 'secteur') echo 'active'; ?>">
					<a href="javascript:;" class="">
					<i class="icon-briefcase"></i> Secteurs
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub">
						<li <?php if($inpage == 'secteur' && $sect == 'add') echo 'class="active"'; ?>>
							<a class="" href="secteur.php?section=add">Ajouter
							</a>
						</li>
						<li <?php if($inpage == 'secteur' && $sect == 'list') echo 'class="active"'; ?>><a class="" href="secteur.php?section=list">Liste</a></li>
					</ul>
				</li>

				<li class="has-sub <?php if($inpage == 'prefecteur') echo 'active'; ?>">
					<a href="javascript:;" class="">
					<i class="icon-group"></i> Préfectures
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub">
						<li <?php if($inpage == 'prefecteur' && $sect == 'add') echo 'class="active"'; ?>>
							<a class="" href="prefecteur.php?section=add">Ajouter
							</a>
						</li>
						<li <?php if($inpage == 'prefecteur' && $sect == 'list') echo 'class="active"'; ?>><a class="" href="prefecteur.php?section=list">Liste</a></li>
					</ul>
				</li>
				<li class="has-sub <?php if($inpage == 'archive') echo 'active'; ?>">
					<a href="javascript:;" class="">
					<i class="icon-bar-chart"></i> Archives
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub">
						<li <?php if($inpage == 'archive' && $sect == 'list') echo 'class="active"'; ?>><a class="" href="archive.php?section=list">Liste</a></li>
						<!--<li class="">
							<a class="" href="search.php?section=search">Chercher
							</a>
						</li>-->
					</ul>
				</li>
				<li class="has-sub">
					<a OnClick="return confirm('Voulez vous vraiment vous déconnecter ?')" href="login.php?section=logout">
					<i class="icon-off"></i> Déconnexion
					</a>					
				</li>

				
			</ul>