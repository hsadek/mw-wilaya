<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/lightbox/bootstrap-lightbox.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<style type="text/css">
	  #date1{
      float:right; 
      font-size: 18px; 
      line-height:37px; 
      color:#FFFFFD; 
      margin-right:5px;
			}
	</style>
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'presse';
		$sect = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('config.php');
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							La Liste	
							<small>
								<?php
			                     	if($section=='list'){ echo "revue de presse";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="historique_envoie.php?section=list">Historique d'envois</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="historique_article.php?section=liste&id=<?php echo $_GET['id']?>">Historique de consultation</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->

		


				<div class="row-fluid">
					
					<div class="span12">
						
					<?php 
					if (isset($_GET["msg"])){
						$msg = htmlspecialchars($_GET["msg"]);
					if($section == 'liste' && $msg == 'update'){
									$alert = 'success';
									$msgs = 'Votre Alerte a été bien planifié.';
								} 
					if($section == 'list' && $msg == 'del'){
									$alert = 'success';
									$msgs = 'Votre Titre a été bien supprimé.';
								} 
					if($section == 'list' && $msg == 'sent'){
									$alert = 'success';
									$msgs = 'Votre Mail a été envoyé avec succès.';
								} 
								?>
									<?php
								if (isset($msg)){
							?>
								<div class="alert alert-<?php echo $alert; ?> alert-dismissable">
								  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  	<center><?php echo $msgs; ?></center>
								</div>
							<?php
								}}
							?>

						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="portlet box blue">
							<div class="portlet-title" style="float:left;">
		                        <h4><i class="icon-reorder"></i>

		                        	<?php 
								if (isset($_GET['idR'])) {
								  $id_pbDate=htmlspecialchars($_GET['idR']);
								  $reqDate='SELECT * FROM `titreevent` WHERE `id`='.$id_pbDate;
								  $resDateQuery=mysql_query($reqDate);
								  $resDate=mysql_fetch_assoc($resDateQuery);
								  $dateNoTime=date('d/m/Y',strtotime($resDate['date']));

								  		$nomD=htmlspecialchars($_GET['nomD']);
									}
							 ?>
		               	Revue de presse N° <?php echo  $resDate['id']  ?> | <?php echo $dateNoTime; ?>
		                        </h4>
		                     </div> 
		                     	<div id="date1"> Nom : <?php echo $nomD; ?></div>
		                     <div class="portlet-body form">  
		                     	
										<?php

										include('config.php');

											if (isset($_POST["filtre"])) {

												$controller2 = htmlspecialchars($_POST["filtre"]);

												if($controller2=='critere'){

												$sortBy = htmlspecialchars($_POST["event"]);
											
												$req='SELECT *  FROM `titreevent` WHERE  DATE(`date`)  ="'.$sortBy.'"';
												$res=mysql_query($req)  or die (mysql_error());

												$req1='SELECT count(id) as compte  FROM `titreevent` WHERE DATE(`date`) ="'.$sortBy.'"';
												$res1=mysql_query($req1)  or die (mysql_error());
												$count=mysql_fetch_assoc($res1);
												$compte=$count['compte'];
									           
											}
											
											}
												                      ?> 		                     
		                        <?php if (!empty($compte)) {?>
		                     	<p>Nous avons trouvé <strong><?php echo $compte ; ?></strong> articles ..</p>
		                     	<?php  } ?>

		                     		<table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">N° Article</th>
			            							<th style="text-align:center;">Titre</th>
			            							<th style="text-align:center;">Date consultation</th>
			            							
			  									
			            							
			            						</tr>
<?php 


	if (isset($_POST["filtre"])) {

		$controller2 = htmlspecialchars($_POST["filtre"]);

		if($controller2=='critere'){

		$sortBy = htmlspecialchars($_POST["event"]);
	
		$req='SELECT *  FROM `titreevent` WHERE  DATE(`date`)  ="'.$sortBy.'"';
		$res=mysql_query($req)  or die (mysql_error());

		$req1='SELECT count(id) as compte  FROM `titreevent` WHERE DATE(`date`) ="'.$sortBy.'"';
		$res1=mysql_query($req1)  or die (mysql_error());
		$count=mysql_fetch_assoc($res1);
		$compte=$count['compte'];
		
		
			           
	}
	
	}



	else {
	  $idR 		= $_GET['idR'];
	  $idD 		= $_GET['idD'];
	  $reqLogsRevue='SELECT * FROM `logs_article` WHERE idR ="'.$idR.'" AND idD ="'.$idD.'"';
	  $resLogsRevue=mysql_query($reqLogsRevue);
	  

		}

	while ($rowLogsRevue=mysql_fetch_assoc($resLogsRevue)) {
	$idArticles = $rowLogsRevue['id_article'];
	$reqTbRevue='SELECT * FROM  `article` WHERE id ="'.$idArticles.'"';
	
	$resTbRevue=mysql_query($reqTbRevue)  or die (mysql_error());
    $rowTbRevue=mysql_fetch_assoc($resTbRevue);



	?>
												<tr>
			            							<td><?php echo $rowTbRevue['id']?></td>
			            							<td><?php echo $rowTbRevue['titre']?></td>
			            							<td><?php echo $rowLogsRevue['date']?></td>
			            						</tr>
	

		<?php } ?>      
	</table>
		                     </div>

		                     	

		                     <?php
		                }
		                else{
		                ?>
							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php	
		                }
		                ?> 
		                </div>
		                   
					</div>
		<?php
		                     		if($section=='remove'){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if (isset($section) && isset($id)){
											if($section=='remove'){							
												mysql_query('delete from `titreevent` where id='.$id);
												//header('location : secteur.php?section=list&msg=ok');
												echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; listeTitre.php?section=list&msg=del" /> ';
											}
										}
									}
		                     	?> 	  
					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/lightbox/bootstrap-lightbox.min.js"></script>
    

	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();

			//$('#demoLightbox').lightbox(options);
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>