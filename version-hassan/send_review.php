<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="assets/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'presse';
		$sect = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							Revue de presse
							<small>
								<?php
			                     	if($section=='send'){ echo "Formulaire d'envoi ";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="listeTitre.php?section=list">Liste revue de presse</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="alerte.php?section=search">Envoyer revue de presse</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						

						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="portlet box blue">
							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
		                        	<?php
			                     	if($section=='send'){ echo "Envoyer revue de presse";}
			                     	?>
		                        </h4>
		                     </div>

		                     <div class="portlet-body form">
		                     	<?php
		                     		if($section=='send'){
		                     	?>
		                     	 <p style="text-align:center">Lorem upsum Lorem upsum Lorem upsumLorem upsum </p>
								<form class="form-horizontal" action="codes.php" method="post">
									<input name="idMailTitre" value="idMailTitre" type="hidden">
									<input name="idOfTitre" value="<?php echo $_GET['id'] ?>" type="hidden">
			                     	<div class="control-group">
		                             
		                           </div>

									<div class="control-group">
		                              <label class="control-label">E-mail</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="email" data-placeholder="Sélectionner un E-mail" class="span6 m-wrap">
		                                    <option value="tout">Sélectionner un E-mail</option>
		                                 	<?php
		                                 		include('config.php');
				                           		$nomEmail = mysql_query('select * from destinataires');
				            					if(mysql_num_rows($nomEmail) == 0){
				            						echo'<option value="">Aucun site web trouvé</option>';
				            					}else{
				            						while ($s = mysql_fetch_assoc($nomEmail)) {
		            									echo '<option value="'.$s['id'].'">'.$s['nom'].' - '.$s['email'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>
		                          
									<div class="form-actions">
										 <script type="text/javascript">
		                              $("#btn-next").click(function(event) {
   									    $('#tab li.active').next().find('a[data-toggle="tab"]').click();
   											 return false;
     
											});
		                              </script>
		                               
		                               <a class="btn btn-primary" id="btn-next" href="mail_preview.php?section=preview&id=<?php echo $_GET['id'] ?>">Visualiser</a>
		                               <button class="btn blue" type="submit">Envoyer</button>

		                           </div>

		                        </form>  
		                     	<?php
		                     		} // End of search
		                     	?>
		                     	<?php
		                     	include('config.php');	

								$req11='SELECT * FROM `alerte` WHERE `date`=NOW()';
								$res11=mysql_query($req11); 
								$tab = mysql_fetch_assoc($res11);

								$log='SELECT email FROM `login`';
								$resLog=mysql_query($log); 
								$tabLog=mysql_fetch_assoc($resLog);

								

								if($section=='search'){

		                     			include('config.php');
		                     			function sel_option($val1, $val2){
										    if ($val1 == $val2){
												return "selected";
											}else{
												return "";
											}
										}

			            								?>
						                              </div>
						                           	</div>
			  
		            								<?php
		                     				
											}
										
		                     			
		                     		 // End of result
		                     	?>
		                     				
		                     </div>
		                     <?php
		                }
		                else{
		                ?>
							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php	
		                }
		                ?> 
		                </div>
		                   
					</div>

					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>

	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>