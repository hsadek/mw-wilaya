<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'archive';
		$sect = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>
						<h3 class="page-title">
							Archive
							<small>
								<?php 
								if($section=='add'){ echo "Ajouter un archive";}
			                    if($section=='list'){ echo "Gérer la liste des archives";}
			                    if($section=='remove'){ echo "Supprimer un archive";}
			                    if($section=='update'){ echo "Mise à jour un archive";}
								?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.php">Tableau de bord</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="archive.php?section=list">Archive</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->

					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						


						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
							<?php
								if (isset($_GET["msg"]))		
								$msg = htmlspecialchars($_GET["msg"]);
								if($section == 'list' && $msg == 'ok'){
									$alert = 'success';
									$msgs = 'Votre archive a été bien ajouté.';
								}elseif($section == 'list' && $msg == 'del'){
									$alert = 'success';
									$msgs = 'Votre archive a été bien supprimé.';
								}elseif($section == 'list' && $msg == 'update'){
									$alert = 'success';
									$msgs = 'Votre archive a été bien modifié..';
								}
							?>
							<?php
								if (isset($msg)){
							?>
								<div class="alert alert-<?php echo $alert; ?> alert-dismissable">
								  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  	<center><?php echo $msgs; ?></center>
								</div>
							<?php
								}
							?>
						<div class="portlet box blue">

							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
			                     	<?php
			                     		if($section=='add'){ echo "Ajouter un archive";}
			                     		if($section=='list'){ echo "Liste des archives";}
			                     		if($section=='remove'){ echo "Supprimer un archive";}
			                     		if($section=='update'){ echo "Mise à jour un archive";}
			                     	?>
		                        </h4>
		                    </div>

		                     <div class="portlet-body form">

		                     	<?php
		                     	include('config.php');
		                     		if($section=='list'){
										$page = (!isset($_GET['p']) ? 1 : intval($_GET['p']));
										$page =(intval($_GET['p'])<=0 ? 1 : $page );
										$pnum = 10;
										$limit = ($page * $pnum) - $pnum;

		                     			$archives = mysql_query('select * from archive order by id desc limit '.$limit.','.$pnum.'');
		            					if(mysql_num_rows($archives) == 0){
		            						echo 'aucun archive trouvé';
		            					}else{
		            						include('pie.php');
		            						echo '
		            						<table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">#</th>
			            							<th style="text-align:center;">Titre d\'archive</th>
			            							<th style="text-align:center;">Lien en ligne</th>
			            							<th style="text-align:center;">Lien local</th>
			            							<th style="text-align:center;">Date</th>
			            							<th style="text-align:center;">Site web</th>
			            							<th style="text-align:center;">Secteur</th>
			            							<th style="text-align:center;">Préfecture</th>
			            							<th style="text-align:center;">Mot clé</th>
			            							<th style="text-align:center;">Action</th>
			            						</tr>
		            						';
		            						$i = (($page * $pnum)-$pnum)+1;
		            						while ($a = mysql_fetch_assoc($archives)) {
		            							$link = mysql_fetch_assoc(mysql_query('select * from siteweb where id="'.$a['linkid'].'"'));
		            							$secteur = mysql_fetch_assoc(mysql_query('select * from secteur where id="'.$a['sid'].'"'));
		            							$prefecteur = mysql_fetch_assoc(mysql_query('select * from prefecteur where id="'.$a['prefecteur_id'].'"'));

		            							echo '
		            							<div id="myModal_'.$a['id'].'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												 	<div class="modal-header">
												 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												 		<h3 id="myModalLabel">'.htmlspecialchars_decode($a['titrearticle']).'</h3>
												 	</div>
												 	<div class="modal-body">
												 		<p><strong>Date :</strong> '.$a['datetime'].'</p>
												 		<p><strong>Siteweb :</strong> '.$link['name'].'</p>
												 		<p><strong>Secteur :</strong> '.utf8_encode($secteur['name']).'</p>
												 		<p><strong>Préfecture :</strong> '.utf8_encode($prefecteur['name']).'</p>
												 		<p><strong>Clé :</strong> '.$a['cle'].'</p>
												 		<p><strong>Lien :</strong> <a href="'.$a['url'].'" target="_blank">Cliquez ici</a></p>
												 		<p><strong>Description :</strong> '.htmlspecialchars_decode($a['description']).'</p>
												 	</div>
												 	<div class="modal-footer">
												 		<button class="btn" data-dismiss="modal" aria-hidden="true">Fermer</button>
													</div>
												</div>
												<tr>
			            							<td>'.$i.'</td>
			            							<td><a href="#myModal_'.$a['id'].'" data-toggle="modal" data-original-title="Description" data-trigger="hover" class="tooltips">'.htmlspecialchars_decode($a['titrearticle']).'</a></td>
			            							<td style="text-align:center;"><a class="btn blue" href="'.$a['url'].'" target="_blank"><i class="icon-link"></i></a></td>
			            							<td style="text-align:center;"><a class="btn blue" href="./archive/'.md5($a['url']).'.html" target="_blank"><i class="icon-link"></i></a></td>
			            							<td>'.$a['datetime'].'</td>
			            							<td>'.$link['name'].'</td>
			            							<td>'.utf8_encode($secteur['name']).'</td>
			            							<td>'.utf8_encode($prefecteur['name']).'</td>
			            							<td>'.$a['cle'].'</td>
			            							<td style="text-align:center;"><a class="btn red" OnClick="return confirm(\'Voulez vous vraiment supprimer cette archive ?\')" href="archive.php?section=remove&id='.$a['id'].'"><i class="icon-trash"></i></a> 
			            								<a class="btn blue" href="archive.php?section=update&id='.$a['id'].'"><i class="icon-pencil"></i></a>
			            							</td>
			            						</tr>
		            							';
		            							$i++;
		            						}
		            						echo '</table>';
											$pagequery = mysql_fetch_assoc(mysql_query('select count(*) as pg from archive'));
											$tp = $pagequery["pg"];
											$actual_link = '?section=list';
											$totalp = ceil($tp/$pnum);
											$totalpr = $totalp - 1;
											$prev = $page - 1;
											$next = $page + 1;
											echo '<div class="pagination pagination-centered">
													<ul>';
													if($page > 1){
														echo '<li><a href="'.$actual_link.'&p='.$prev.'">&laquo;</a></li>';
													}else{
														echo '<li class="disabled"><a href="#">&laquo;</a></li>';
													}
													
													if($totalp < 7 + ($pnum * 2)){
														for($i=1;$i<=$totalp;$i++){
														if($i == $page)
															echo '<li class="active"><a href="#">'.$i.'</a></li>';
														else
															echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
														}
													}elseif($totalp > 5 + ($pnum * 2)){
														if($page < 1 + ($pnum * 2)){
															for($i=1;$i<=4+($pnum * 2);$i++){
															if($i == $page)
																echo '<li class="active"><a href="#">'.$i.'</a></li>';
															else
																echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
															}
															echo '<li><a href="#">...</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalpr.'">'.$totalpr.'</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalp.'">'.$totalp.'</a></li>';
														}elseif($totalp - ($pnum * 2) > $page && $page > ($pnum * 2)){
															echo '<li><a href="'.$actual_link.'&p=1">1</a></li>';
															echo '<li><a href="'.$actual_link.'&p=2">2</a></li>';
															echo '<li><a href="#">...</a></li>';
															for($i=$page-$pnum;$i<=$page+$pnum;$i++){
															if($i == $page)
																echo '<li class="active"><a href="#">'.$i.'</a></li>';
															else
																echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
															}
															echo '<li><a href="#">...</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalpr.'">'.$totalpr.'</a></li>';
															echo '<li><a href="'.$actual_link.'&p='.$totalp.'">'.$totalp.'</a></li>';
														}else{
															echo '<li><a href="'.$actual_link.'&p=1">1</a></li>';
															echo '<li><a href="'.$actual_link.'&p=2">2</a></li>';
															echo '<li><a href="#">...</a></li>';
															for($i=$totalp-(2+($pnum * 2));$i<=$totalp;$i++){
															if($i == $page)
																echo '<li class="active"><a href="#">'.$i.'</a></li>';
															else
																echo '<li><a href="'.$actual_link.'&p='.$i.'">'.$i.'</a></li>';
															}
														}
													}
													
													if($page < $totalp){
														echo '<li><a href="'.$actual_link.'&p='.$next.'">&raquo;</a></li>';
													}else{
														echo '<li class="disabled"><a href="#">&raquo;</a></li>';
													}
													echo'
													</ul>
												</div>';
			            				}
		                     		}
		                     	?>


		                     	<?php
		                     		if($section=='remove'){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if (isset($section) && isset($id)){
											if($section=='remove'){							
												mysql_query('delete from archive where id="'.$id.'"');
												//header('location : secteur.php?section=list&msg=ok');
												echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; archive.php?section=list&msg=del" /> ';
											}
										}
									}
		                     	?> 	

		                     	<?php
		                     		if($section=='update'){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if(isset($id)){
		                     				$ar = mysql_fetch_assoc(mysql_query('select * from archive where id="'.$id.'"'));
		                     	?>
		                     	
		                     	<form class="form-horizontal" action="codes.php" method="GET">
		                     		<input name="controller" value="updatearchive" type="hidden">
									<input name="id" value="<?php echo $ar['id']; ?>" type="hidden">
									<div class="control-group">
		                              <label class="control-label">Titre d'archive </label>
		                              <div class="controls">
		                                 <input type="text" readonly name="titrearticle" value="<?php echo $ar['titrearticle']; ?>" data-original-title="Inserer le titre d'archive" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>
		                           <div class="control-group">
		                              <label class="control-label">Siteweb</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="link" data-placeholder="Sélectionner un siteweb" class="span6 m-wrap">
		                                    <option value="">Sélectionner un siteweb...</option>

		                                 	<?php
		                                 		function sel_option($val1, $val2){
												    if ($val1 == $val2){
												        return "selected";
												    }else{
												        return "";
												    }
												}
				                           		$sitewebs = mysql_query('select * from siteweb');
				            					if(mysql_num_rows($sitewebs) == 0){
				            						echo'<option value="">Aucun siteweb trouvé</option>';
				            					}else{
				            						while ($s = mysql_fetch_assoc($sitewebs)) {
		            									echo '<option value="'.$s['id'].'" '.sel_option($s['id'],$ar['linkid']).'>'.$s['name'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>
		                           <div class="control-group">
		                              <label class="control-label">Secteur</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="sid" data-placeholder="Sélectionner un secteur" class="span6 m-wrap">
		                                    <option value="">Sélectionner un secteur...</option>

		                                 	<?php
				                           		$secteurs = mysql_query('select * from secteur');
				            					if(mysql_num_rows($secteurs) == 0){
				            						echo'<option value="">Aucun secteur trouvé</option>';
				            					}else{
				            						while ($se = mysql_fetch_assoc($secteurs)) {
		            									echo '<option value="'.$se['id'].'" '.sel_option($se['id'],$ar['sid']).'>'.$se['name'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>
		                           <div class="control-group">
		                              <label class="control-label">Préfecture</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="pid" data-placeholder="Sélectionner un préfecture" class="span6 m-wrap">
		                                    <option value="">Sélectionner un préfecture...</option>

		                                 	<?php
				                           		$prefecteurs = mysql_query('select * from prefecteur');
				            					if(mysql_num_rows($prefecteurs) == 0){
				            						echo'<option value="">Aucun préfecture trouvé</option>';
				            					}else{
				            						while ($prefecteur = mysql_fetch_assoc($prefecteurs)) {
		            									echo '<option value="'.$prefecteur['id'].'" '.sel_option($prefecteur['id'],$ar['prefecteur_id']).'>'.$prefecteur['name'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>
		                           <div class="control-group">
		                              <label class="control-label">Clé d'archive </label>
		                              <div class="controls">
		                                 <input type="text" readonly name="key" value="<?php echo $ar['cle']; ?>" data-trigger="hover" class="span6 m-wrap">                          
		                              </div>
		                           </div>

		                           	<div class="form-actions">
		                              <button class="btn blue" type="submit">Enregistrer</button>
		                           </div>

		                        </form>   
		                     	<?php
		                     			}
		                     		} // End of ADD
		                     	?>


		                     </div>
		                <?php
		                }
		                else{
		                ?>
							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php	
		                }
		                ?>     


		                </div>
		                   
					</div>

					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>