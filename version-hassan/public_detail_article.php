<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/lightbox/bootstrap-lightbox.min.css" rel="stylesheet">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
.thumbnail{

	max-width: 50%;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('sidebar_public.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							
						?>		
						<h3 class="page-title">
							Article
							<small>

							Détails d'article
			                  
							</small>
						</h3>
						
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						


						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
							
					
								
							<?php
							

								include('config.php');

	if (isset($_GET["id"])) {

		$controller2 = htmlspecialchars($_GET["id"]);

	
		$req='SELECT *  FROM `article` WHERE `id` ="'.$controller2.'"';
		$res=mysql_query($req)  or die (mysql_error());
		$row=mysql_fetch_assoc($res);
		
		
			}  


	if (isset($_GET["id"]) && isset($_GET["idR"]) && isset($_GET["idD"])) {

		$numArticle=htmlspecialchars($_GET["id"]);
		$numDstinataire=htmlspecialchars($_GET["idD"]);
		$idR=htmlspecialchars($_GET["idR"]);
		//$numArticle=htmlspecialchars($_GET["id"]);


		$req1='SELECT * FROM `logs_article`  WHERE `id_article`='.$numArticle.' AND `idD`='.$numDstinataire;
		$res1=mysql_query($req1)  or die (mysql_error());
		$row1=mysql_fetch_assoc($res1);
		
		//while ($row1=mysql_fetch_assoc($res1)){

		if (!$row1) {
		$req2='INSERT INTO `logs_article`(`id_article`, `idD`, `idR`) VALUES ('.$numArticle.','.$numDstinataire.','.$idR.')';
		$res2=mysql_query($req2)  or die (mysql_error());
				
			}
			
		//}

	
	
		
		
			}        
							?>
						<div class="portlet box blue">
							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
		                        	
			                     	Détails d'article
			                     	
		                        </h4>
		                     </div>
		                     <style type="text/css">
		                      input.span6.m-wrap.tooltips
		                      ,textarea.span6.m-wrap.tooltips{
		                      	width: 100%;
		                      }
		                     </style>
		                     <div class="portlet-body form">
		                     	
								<form  action="codes.php" method="post" enctype="multipart/form-data">
									<input name="section" value="result" type="hidden">
							    	<input name="modArticle" value="modArticle" type="hidden">
							    	<input name="idArticle" value="<?php echo $_GET['id'];?>" type="hidden">
							    	<input name="idtitre" value="<?php echo $row['titreevent'];?>" type="hidden">
			                     	<div class="control-group">
		                              <label class="control-label">Titre</label>
		                              <div class="controls">
		                                 <input type="text"  value="<?php echo $row['titre']; ?>" name="titre" data-original-title="Inserer votre titre" data-trigger="hover" class="span6 m-wrap tooltips"  disabled>                       
		                              </div>
		                           </div>

									<div class="control-group">
		                              <label class="control-label">Synthése</label>
		                              <div class="controls">
		                                 <textarea name="Synthese" value="" rows="10" data-original-title="Synthése" data-trigger="hover" class="span6 m-wrap tooltips"  disabled ><?php echo $row['synthese']; ?></textarea>                       

		                              </div>
		                           </div>
									
									
		                           	
		                              <label class="control-label">Image</label>
		                              <div class="form-horizontal">
		                              <div class="controls">
		                              	<a data-toggle="lightbox" href="#demoLightbox" class="thumbnail">
        													<img src="<?php echo $row['url']?>" alt="Click to view the lightbox">
      													</a>
			            								<div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
																	<div class='lightbox-content'>
			            												<img src="<?php echo $row['url']?>"/>
			            											</div>
			            			  </div>				</div>
		                              </div>
		                           
		                              <div class="form-actions">
		                                
		                              
		                           </div>
		                           </div>
								
		                        </form>  
		                     	
		                     
		                     				
		                     </div>
		                  
		              
		                </div>
		                   
					</div>

					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="assets/lightbox/bootstrap-lightbox.min.js"></script>

	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>