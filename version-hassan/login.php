<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title>Monitoring Words :: by :: JAWEB.ma</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/metro.css" rel="stylesheet" />
  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="assets/css/style.css" rel="stylesheet" />
  <link href="assets/css/style_responsive.css" rel="stylesheet" />
  <link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
  <link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
  <link rel="shortcut icon" href="favicon.ico" />
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
	<?php
		include('config.php');
		session_start();
		if (isset($_GET["section"])){
			$section = htmlspecialchars($_GET["section"]);
			if($section == 'login'){
				$user = htmlspecialchars($_POST["username"]);
				$pass = htmlspecialchars($_POST["userpass"]);
				if(isset($user) && !empty($user) && isset($pass) && !empty($pass)){
					$sql = mysql_query('select name,pass from login where name="'.$user.'" and pass="'.md5($pass).'"');
					$num = mysql_num_rows($sql);
					if($num > 0){
						$row = mysql_fetch_assoc($sql);
						$username = $row['name'];
						$userpass = $row['pass'];
						$_SESSION['uname'] 	= $username;
						$_SESSION['upass'] 	= $userpass;
						?>
              <script type="text/javascript">
                window.location = "index.php";
              </script>
            <?
					}else{
						$_SESSION['error'] = 'Incorrect Nom d\'utilisateur ou Mot de passe!';
						header("Location: ./login.php");
					}
				}else{
					$_SESSION['error'] = 'Veuillez inserer le Nom d\'utilisateur et le Mot de passe!';
					header("Location: ./login.php");
				}
			}elseif($section == 'logout'){
				session_destroy();
				header("Location: ./login.php");
			}
		}else{
	?>
  <!-- BEGIN LOGO -->
  <div class="logo">
    <img src="./assets/img/logo.png" alt="" /> 
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="form-vertical login-form" method="POST" action="login.php?section=login" />
		<?php if(isset($_SESSION['error'])){ ?>
				<div class="alert alert-error">
					<a class="close" data-dismiss="alert" href="#"></a><?php echo $_SESSION['error']; ?>
				</div>
				<?php } ?>
      <h3 class="form-title">Connexion</h3>
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-user"></i></span><input name="username" class="m-wrap" id="username" type="text" placeholder="Nom d'utilisateur">
				</div>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on"><i class="icon-lock"></i></span><input name="userpass" class="m-wrap" id="userpass" type="password" placeholder="Mot de passe">
				</div>
			</div>
		</div>
      <div class="form-actions">
        <!--<label class="checkbox">
        <input type="checkbox" /> Remember me
        </label>-->
        <button type="submit" id="login-btn" class="btn blue pull-right">
        Login <i class="m-icon-swapright m-icon-white"></i>
        </button>            
      </div>
      <!--<div class="forget-password">
        <h4>Forgot your password ?</h4>
        <p>
          no worries, click <a href="javascript:;" class="" id="forget-password">here</a>
          to reset your password.
        </p>
      </div>-->
    </form>
    <!-- END LOGIN FORM -->        
    <!-- BEGIN FORGOT PASSWORD FORM -
    <form class="form-vertical forget-form" action="index.html" />
      <h3 class="">Forget Password ?</h3>
      <p>Enter your e-mail address below to reset your password.</p>
      <div class="control-group">
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-envelope"></i>
            <input class="m-wrap" type="text" placeholder="Email" />
          </div>
        </div>
      </div>
      <div class="form-actions">
        <a href="javascript:;" id="back-btn" class="btn">
        <i class="m-icon-swapleft"></i>  Back
        </a>
        <a href="javascript:;" id="forget-btn" class="btn green pull-right">
        Submit <i class="m-icon-swapright m-icon-white"></i>
        </a>            
      </div>
    </form>
     END FORGOT PASSWORD FORM -->
	<?php
		session_unset('error');
	}
	?>
  </div>
  <!-- END LOGIN -->
  <!-- BEGIN COPYRIGHT -->
  <div class="copyright">
	&copy; 2015 <a href="www.jaweb.ma"> JAWEB</a>
  </div>
  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
  <script src="assets/js/jquery-1.8.3.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>  
  <script src="assets/uniform/jquery.uniform.min.js"></script> 
  <script src="assets/js/jquery.blockui.js"></script>
  <script src="assets/js/app.js"></script>
  <script>
    jQuery(document).ready(function() {     
      App.initLogin();
    });
  </script>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37564768-1']);
    _gaq.push(['_setDomainName', 'keenthemes.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>