<?php
function curl_get($url, $params)
{
    $post_params = array();
    foreach ($params as $key => &$val) {
      if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);
 
    $fullurl = $url."?".$post_string;
 
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_URL, $fullurl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mailana (curl)');
    $result = curl_exec($ch);
    curl_close($ch);
 
    return $result;
}
 
 
function perform_google_web_search($termstring,$siteweb = null)
{
    $start = 1;
    $result = array();
    while ($start<50)
    {
        $searchurl = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0';
        $searchurl .= '&key=AIzaSyB2_0XqfLFWauSAZOmtKakZ9S1RBBiC5UU';
        $searchurl .= '&start='.$start;
        $searchurl .= '&rsz=large';
        $searchurl .= '&filter=0';
        $searchurl .= '&q='.urlencode($termstring).$siteweb;
        $response = curl_get($searchurl, array());
       
        $responseobject = json_decode($response, true);
       
        if (count($responseobject['responseData']['results'])==0)
            break;
       
        $allresponseresults = $responseobject['responseData']['results'];
 
        foreach ($allresponseresults as $responseresult)
        {
            $result[] = array(
                'url' => $responseresult['url'],
                'title' => $responseresult['title'],
                'abstract' => $responseresult['content'],
            );

        }
 
        $start += 8;
    }
 
    return $result;
}
?>