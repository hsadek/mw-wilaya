<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'search';
		$sect = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							Rechercher
							<small>
								<?php
			                     	if($section=='search'){ echo "Formulaire de recherche";}
			                     	if($section=='result'){ echo "Résultats de recherche";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.php">Tableau de bord</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="search.php?section=search">Rechercher</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						


						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="portlet box blue">
							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
		                        	<?php
			                     		if($section=='search'){ echo "Formulaire de recherche";}
			                     		if($section=='result'){ echo "Résultats de recherche";}
			                     	?>
		                        </h4>
		                     </div>

		                     <div class="portlet-body form">
		                     	<?php
		                     		if($section=='search'){
		                     	?>
								<form class="form-horizontal" action="search.php" method="GET">
									<input name="section" value="result" type="hidden">
			                     	<div class="control-group">
		                              <label class="control-label">Mot clé</label>
		                              <div class="controls">
		                                 <input type="text" name="key" data-original-title="Inserer votre mot clé" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>

									<div class="control-group">
		                              <label class="control-label">Site web</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="siteweb" data-placeholder="Sélectionner un site web" class="span6 m-wrap">
		                                    <option value="">Sélectionner un site web...</option>
		                                    <option value="0">Tout les sites</option>

		                                 	<?php
		                                 		include('config.php');
				                           		$sitewebs = mysql_query('select * from siteweb');
				            					if(mysql_num_rows($sitewebs) == 0){
				            						echo'<option value="">Aucun site web trouvé</option>';
				            					}else{
				            						while ($s = mysql_fetch_assoc($sitewebs)) {
		            									echo '<option value="'.$s['id'].'">'.$s['name'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>

									<div class="form-actions">
		                              <button class="btn blue" type="submit">Rechercher</button>
		                              <button class="btn" type="button">Annuler</button>
		                           </div>

		                        </form>  
		                     	<?php
		                     		} // End of search
		                     	?>
		                     	<?php
		                     		if($section=='result'){
		                     			include('config.php');
		                     			function sel_option($val1, $val2){
										    if ($val1 == $val2){
												return "selected";
											}else{
												return "";
											}
										}
		                     			$key = htmlspecialchars($_GET["key"]);
		                     			$siteweb = htmlspecialchars($_GET["siteweb"]);
		                     			if(isset($key) || !empty($key) && isset($siteweb) || !empty($siteweb)){
		                     				if($siteweb == 0){
		                     					?>
													<div class="tabbable tabbable-custom">
		                     						<ul class="nav nav-tabs">
		                     							<li class="active"><a href="#Tab1" data-toggle="tab">Tous</a></li>
		                     					<?php
		                     						$siteswebs = mysql_query('select * from siteweb');
		                     						$i = 1;
													$sitex = array();
					            					while ($sitesweb = mysql_fetch_assoc($siteswebs)){
			            								echo '<li class=""><a href="#Tab'.($i+1).'" data-toggle="tab">'.$sitesweb['name'].'</a></li>';
			            								//echo'<div class="tab-pane" id="Tab'.($i+1).'">';
			            								//$siteweblink = $sitesweb['link'];
			            								//include('result.php');
														array_push($sitex,$sitesweb['link']);
			            								$i++;
					            					}
					            					echo '</div>';
					            				?>
													</ul>
													<div class="tab-content">
														<div class="tab-pane active" id="Tab1">
															<table class="table table-striped table-bordered">
																<tr>
																	<th width="200" style="text-align:center;">Titre</th>
																	<th width="60" style="text-align:center;">Lien</th>
																	<th width="500" style="text-align:center;">Description</th>
																	<th width="180" style="text-align:center;">Enregistrement</th>
																</tr>
														<?php
															foreach($sitex as $x){
																$siteweblink = $x;
																include('result.php');
															}
														?>		
															</table>
														</div>
		                     						</div>
		                     						</div>
		                     					<?php
		                     				} // End 0
		                     				else{
		                     					$sitesweb = mysql_fetch_assoc(mysql_query('select * from siteweb where id="'.$siteweb.'"'));
		                     					$siteweblink = $sitesweb['link'];
		                     					?>
												<table class="table table-striped table-bordered">
													<tr>
														<th width="200" style="text-align:center;">Titre</th>
														<th width="60" style="text-align:center;">Lien</th>
														<th width="500" style="text-align:center;">Description</th>
														<th width="180" style="text-align:center;">Enregistrement</th>
													</tr>
													<?php
														foreach($sitex as $x) {
															$siteweblink = $x;
															include('result.php');
														}
													?>		
												</table>
												<?php
		                     				}
		                     			}
		                     		} // End of result
		                     	?>
		                     				
		                     </div>
		                     <?php
		                }
		                else{
		                ?>
							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php	
		                }
		                ?> 
		                </div>
		                   
					</div>

					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>