<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/lightbox/bootstrap-lightbox.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'presse';
		$sect = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							 Historique d'envois
							<small>
								<?php
			                     	if($section=='list'){ echo "revue de presse";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.php">Tableau de bord</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="historique_envoie.php?section=list">Historique d'envois</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->

		


				<div class="row-fluid">
					
					<div class="span12">
						
					<?php 
					if (isset($_GET["msg"])){
						$msg = htmlspecialchars($_GET["msg"]);
					if($section == 'liste' && $msg == 'update'){
									$alert = 'success';
									$msgs = 'Votre Alerte a été bien planifié.';
								} 
					if($section == 'list' && $msg == 'del'){
									$alert = 'success';
									$msgs = 'Votre Titre a été bien supprimé.';
								} 
					if($section == 'list' && $msg == 'sent'){
									$alert = 'success';
									$msgs = 'Votre Mail a été envoyé avec succès.';
								} 
								?>
									<?php
								if (isset($msg)){
							?>
								<div class="alert alert-<?php echo $alert; ?> alert-dismissable">
								  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  	<center><?php echo $msgs; ?></center>
								</div>
							<?php
								}}
							?>

						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="portlet box blue">
							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
		                        	 Liste d'historique d'envois
		                        </h4>
		                     </div> 
		                     
		                     <div class="portlet-body form">  
		                     	
										<?php

										include('config.php');

											if (isset($_POST["filtre"])) {

												$controller2 = htmlspecialchars($_POST["filtre"]);

												if($controller2=='critere'){

												$sortBy = htmlspecialchars($_POST["event"]);
											
												$req='SELECT *  FROM `titreevent` WHERE  DATE(`date`)  ="'.$sortBy.'"';
												$res=mysql_query($req)  or die (mysql_error());

												$req1='SELECT count(id) as compte  FROM `titreevent` WHERE DATE(`date`) ="'.$sortBy.'"';
												$res1=mysql_query($req1)  or die (mysql_error());
												$count=mysql_fetch_assoc($res1);
												$compte=$count['compte'];
												
												
													           
											}
											
											}
												                      ?> 		                     
		                        <?php if (!empty($compte)) {?>
		                     	<p>Nous avons trouvé <strong><?php echo $compte ; ?></strong> articles ..</p>
		                     	<?php  } ?>

		                     		<table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">N° Revue</th>
			            							<th style="text-align:center;">Titre</th>
			            							<th style="text-align:center;">Destinataire</th>
			            							<th style="text-align:center;">Etat de consultation</th>
			            							<th style="text-align:center;">Date de consultation</th>
			            							<th style="text-align:center;">Historique de consultation</th>
			            							
			  									
			            							
			            						</tr>
<?php 
include('config.php');

	if (isset($_POST["filtre"])) {

		$controller2 = htmlspecialchars($_POST["filtre"]);

		if($controller2=='critere'){

		$sortBy = htmlspecialchars($_POST["event"]);
	
		$req='SELECT *  FROM `titreevent` WHERE  DATE(`date`)  ="'.$sortBy.'"';
		$res=mysql_query($req)  or die (mysql_error());

		$req1='SELECT count(id) as compte  FROM `titreevent` WHERE DATE(`date`) ="'.$sortBy.'"';
		$res1=mysql_query($req1)  or die (mysql_error());
		$count=mysql_fetch_assoc($res1);
		$compte=$count['compte'];
		
		
			           
	}
	
	}



	else {

  $reqLogsRevue='SELECT * FROM `logs_revue`';
  $resLogsRevue=mysql_query($reqLogsRevue);
		}
$i=1;
while ($rowLogsRevue=mysql_fetch_assoc($resLogsRevue)) {

	// liste des revue
	$reqTbRevue='SELECT * FROM `titreevent` WHERE id ="'.$rowLogsRevue['idR'].'"';
	$resTbRevue=mysql_query($reqTbRevue)  or die (mysql_error());
    $rowTbRevue=mysql_fetch_assoc($resTbRevue);

	// le nom destinataires
	$reqTbDestinataire='SELECT * FROM `destinataires` WHERE id ="'.$rowLogsRevue['idD'].'"';
	$resTbDestinataire=mysql_query($reqTbDestinataire)  or die (mysql_error());
    $rowTbDestinataire=mysql_fetch_assoc($resTbDestinataire);
    $nomD=$rowTbDestinataire['nom'];
    $idD=$rowTbDestinataire['id'];

	$idRDatabase = $rowLogsRevue['idR'];


    // compte logs article
    $reqlogsArticle='SELECT count(id) as compte FROM `logs_article`  WHERE idD ="'.$idD.'" AND idR ="'.$idRDatabase.'" ';
	$reslogsArticle=mysql_query($reqlogsArticle)  or die (mysql_error());
    $rowlogsArticle=mysql_fetch_assoc($reslogsArticle);
    $comptelogsArticle=$rowlogsArticle['compte'];
   
	$getIdR='SELECT idR FROM `logs_article`  WHERE `idD`='.$idD;
	$resgetIdR=mysql_query($getIdR)  or die (mysql_error());
	$rowgetIdR=mysql_fetch_assoc($resgetIdR);


	?>
												<tr>
			            							<td><?php echo $rowTbRevue['id']?></td>
			            							<td><?php echo $rowTbRevue['titre']?></td>
			            							<td><?php echo $rowTbDestinataire['nom']?></td>
			            							<td style="text-align:center;">

			            								<?php 
			            								if ($rowLogsRevue['date']==NULL) {
			            									echo "<div class='btn'>Non vu</a>";
			            								}else{

			            									echo "<div class='btn green'>Vu</a>";
			            								}
			            								

			            								?>

			            							</td>
			            							
			            							<td><?php echo $rowLogsRevue['date']?></td>
			            							<td style="text-align:center;">

			            								<?php 
			            								if ($comptelogsArticle==0) {
			            									echo "<div class='btn deactivated'><i class='fa fa-eye'></i></div>";
			            								}else{?>

<a class='btn blue' href='historique_article.php?section=liste
&idR=<?php echo $rowLogsRevue['idR']?>
	&nomD=<?php echo $nomD; ?>
	&idD=<?php echo $idD; ?>
	'>
	<i class='fa fa-eye'></i></a>
			            							<?php	}?>

			            							</td>
			            						</tr>
	

	<?php

	$i++;} ?>      
		            							</table>
		                     </div>

		                     	

		                     <?php
		                }
		                else{
		                ?>
							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php	
		                }
		                ?> 
		                </div>
		                   
					</div>
		<?php
		                     		if($section=='remove'){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if (isset($section) && isset($id)){
											if($section=='remove'){							
												mysql_query('delete from `titreevent` where id='.$id);
												//header('location : secteur.php?section=list&msg=ok');
												echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; listeTitre.php?section=list&msg=del" /> ';
											}
										}
									}
		                     	?> 	  
					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/lightbox/bootstrap-lightbox.min.js"></script>
    

	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();

			//$('#demoLightbox').lightbox(options);
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>