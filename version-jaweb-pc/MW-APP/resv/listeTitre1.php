<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/lightbox/bootstrap-lightbox.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'presse';
		$sect = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('config.php');
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							La Liste	
							<small>
								<?php
			                     	if($section=='list'){ echo "revue de presse";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.php">Tableau de bord</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="listeTitre.php?section=list">Liste revue de presse</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->

		


				<div class="row-fluid">
					
					<div class="span12">
						
					<?php 
					if (isset($_GET["msg"])){
						$msg = htmlspecialchars($_GET["msg"]);
					if($section == 'liste' && $msg == 'update'){
									$alert = 'success';
									$msgs = 'Votre Alerte a été bien planifié.';
								} 
					if($section == 'list' && $msg == 'del'){
									$alert = 'success';
									$msgs = 'Votre Titre a été bien supprimé.';
								} 

								?>
									<?php
								if (isset($msg)){
							?>
								<div class="alert alert-<?php echo $alert; ?> alert-dismissable">
								  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  	<center><?php echo $msgs; ?></center>
								</div>
							<?php  }}?>
						
						<?php 


								if (isset($_GET["id"])) {

									$id = htmlspecialchars($_GET["id"]);

								$reqRevue='SELECT * FROM `titreevent` WHERE `id`='.$id;
								$res1=mysql_query($reqRevue)  or die (mysql_error());
								$r1=mysql_fetch_assoc($res1);

								}	 
												

						 ?>
						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="portlet box blue">
							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
		                        	 Liste revue de presse
		                        </h4>
		                     </div> 
		                     
		           <div class="portlet-body form">  
   <?php if($section=='list'){ ?>         	
										<?php

									

											if (isset($_POST["filtre"])) {

												$controller2 = htmlspecialchars($_POST["filtre"]);

												if($controller2=='critere'){

												$sortBy = htmlspecialchars($_POST["event"]);
											
												$req='SELECT *  FROM `titreevent` WHERE  DATE(`date`)  ="'.$sortBy.'"';
												$res=mysql_query($req)  or die (mysql_error());

												$req1='SELECT count(id) as compte  FROM `titreevent` WHERE DATE(`date`) ="'.$sortBy.'"';
												$res1=mysql_query($req1)  or die (mysql_error());
												$count=mysql_fetch_assoc($res1);
												$compte=$count['compte'];
												
												
													           
											}
											
											}
												                      ?> 		                     

		                     <form class="form-horizontal" action="listeTitre.php?section=list&sort=filtre" method="post">

		                     	<input name="section" value="result" type="hidden">
							    <input name="filtre" value="critere" type="hidden">

		                    	<div class="control-group">
		                              <div class="controls">
		                                 <select tabindex="1" name="event" data-placeholder="Sélectionner un critère" class="span6 m-wrap" >
		                                    <option value="all">Sélectionner tout</option>
		                                 	<?php
		                                 		include('config.php');
				                           		$event = mysql_query('select * from titreevent');
				            					if(mysql_num_rows($event) == 0){
				            						echo'<option value="">Aucun critère trouvé</option>';
				            					}else{
				            						while ($s = mysql_fetch_assoc($event)) {
		            									echo '<option value="'.date('Y-m-d',strtotime($s['date'])).'">'.date('Y-m-d',strtotime($s['date'])).'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                               <button class="btn blue" type="submit">Filtrer</button>
		                              </div>

		                           </div>
		                       </form>

		                        <?php if (!empty($compte)) {?>
		                     	<p>Nous avons trouvé <strong><?php echo $compte ; ?></strong> articles ..</p>
		                     	<?php  } ?>

		                     		<table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">N°</th>
			            							<th style="text-align:center;">Titre</th>
			            							<th style="text-align:center;">Date</th>
			            							<th style="text-align:center;">Actions</th>
			            							
			  									
			            							
			            						</tr>
<?php 

	if (isset($_POST["filtre"])) {

		$controller2 = htmlspecialchars($_POST["filtre"]);

		if($controller2=='critere'){

		$sortBy = htmlspecialchars($_POST["event"]);
	
		$req='SELECT *  FROM `titreevent` WHERE  DATE(`date`)  ="'.$sortBy.'"';
		$res=mysql_query($req)  or die (mysql_error());

		$req1='SELECT count(id) as compte  FROM `titreevent` WHERE DATE(`date`) ="'.$sortBy.'"';
		$res1=mysql_query($req1)  or die (mysql_error());
		$count=mysql_fetch_assoc($res1);
		$compte=$count['compte'];
		
		
			           
	}
	
	}



	else {

  $req='SELECT * FROM `titreevent`';
  $res=mysql_query($req);
		}
$i=1;
while ($r=mysql_fetch_assoc($res)) {

						if (!empty($_GET['sort'])) {

						 ?>						
						 						<tr>
			            							<td><?php echo $r['id']?></td>
			            							<td><?php echo $r['titre']?></td>
			            							<td><?php echo $r['date']?></td>
			            							<td  style="text-align:center;">
			            							<a class="btn blue" href="listeArti.php?section=liste&id=<?php echo $r['id']?>"><i class="fa fa-eye"></i></a>
			            							<a class="btn orange" href="listeArti.php?section=liste&id=<?php echo $r['id']?>"><i class="fa fa-envelope-o"></i></a>
			            							<a class="btn red" OnClick="return confirm('Voulez vous vraiment supprimer cet article ?')" href="listeTitre.php?section=remove&id=<?php echo $r['id']?>"><i class="icon-trash"></i></a></td>
			            							
			            					
			            							
			            						</tr>
			            	<?php } else {?>

												<tr>
			            							<td><?php echo $r['id']?></td>
			            							<td><?php echo $r['titre']?></td>
			            							<td><?php echo $r['date']?></td>
			            							<td  style="text-align:center;"><a class="btn blue" href="listeArti.php?section=liste&id=<?php echo $r['id']?>"><i class="fa fa-eye"></i></a>
			            							<a class="btn red" OnClick="return confirm('Voulez vous vraiment supprimer cet article  ?')" href="listeTitre.php?section=remove&id=<?php echo $r['id']?>"><i class="icon-trash"></i></a>
			            							<a class="btn btn-danger" href="listeArti.php?section=liste&id=<?php echo $r['id']?>"><i class="fa fa-envelope-o"></i></a>
													</td>
			            						</tr>
	<?php }$i++;} ?>      
		            							</table>
	<?php }  ?>
	 <?php if($section=='list'){ ?>    

	 		<table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">N°</th>
			            							<th style="text-align:center;">Titre</th>
			            							<th style="text-align:center;">Image</th>
			            							<th style="text-align:center;">Actions</th>
			  									
			            							
			            						</tr>
													<?php 
													include('config.php');

														if (isset($_GET["id"])) {

															$id = htmlspecialchars($_GET["id"]);

															$req='SELECT *  FROM `article` WHERE `titreevent`='.$id;
															$res=mysql_query($req)  or die (mysql_error());

															$reqRevue='SELECT `date` FROM `titreevent` WHERE `id`='.$id;
															$res1=mysql_query($reqRevue)  or die (mysql_error());
															$r1=mysql_fetch_assoc($res1);	          
														
														}

														
													$i=1;
													while ($r=mysql_fetch_assoc($res)) {

													?>
							
						 						<tr>
			            							<td><?php echo $r['id']?></td>
			            							<td><?php echo $r['titre']?></td>
			            							<td>
			            								<a data-toggle="lightbox" href="#demoLightbox<?php echo $i; ?>" class="thumbnail">
        													<img src="<?php echo $r['url']?>" width="100" height="100" alt="Click to view the lightbox">
      													</a>
			            								<div id="demoLightbox<?php echo $i; ?>" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
																	<div class='lightbox-content'>
			            												<img src="<?php echo $r['url']?>"/>
			            											</div>
			            								</div>
			            							</td>
			            							<td style="text-align:center;">
			            								<a class="btn blue" href="detailArticle.php?section=liste&id=<?php echo $r['id']?>"><i class="icon-pencil"></i></a>
			            								<a class="btn red" OnClick="return confirm('Voulez vous vraiment supprimer cette titre ?')" href="listeArti.php?section=remove&id=<?php echo $r['id']?>"><i class="icon-trash"></i></a>
			            								<a class="btn blue"  href="fullArti.php?section=liste&id=<?php echo $r['id']?>"><i class="fa fa-eye"></i></a>
			            							</td>
			            						</tr>
			            
				<?php $i++;} ?>      
		            							</table>
		                    <?php } ?>
		                    </div>

		                     	

		                     <?php } else{?>

							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php	
		                }
		                ?> 
		                </div>
		                   
					</div>
		<?php
		                     		if($section=='remove'){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if (isset($section) && isset($id)){
											if($section=='remove'){							
												mysql_query('delete from `titreevent` where id='.$id);
												//header('location : secteur.php?section=list&msg=ok');
												echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; listeTitre.php?section=list&msg=del" /> ';
											}
										}
									}
		                     	?> 	  
					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/lightbox/bootstrap-lightbox.min.js"></script>
    

	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();

			//$('#demoLightbox').lightbox(options);
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>