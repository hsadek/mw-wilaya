<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/lightbox/bootstrap-lightbox.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		$inpage = 'presse';
		$des = 'des';
		$section = $_GET['section'];
	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('config.php');
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							<?php if($section=='list'){ echo "Destinataires";} ?>
							<?php if($section=='add'){ echo "Destinataires";} ?>
							<small>
								<?php
			                     	if($section=='list'){ echo "Destinataires";}
			                     	if($section=='add'){ echo "Formulaire d'ajout";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
						<?php if($section=='list'){?>
							<li>
								<i class="icon-home"></i>
								<a href="index.php">Tableau de bord</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="destinataires.php?section=list">Liste des destinataires</a> 
							</li>
						<?php }?>
						<?php if($section=='add'){?>
							<li>
								<i class="icon-home"></i>
								<a href="destinataires.php?section=list">Liste des destinataires</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="destinataires.php?section=add">Ajouter un destinataire</a> 
							</li>
						<?php }?>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->

		


				<div class="row-fluid">
					
					<div class="span12">
						
					<?php 
					if (isset($_GET["msg"])){
						$msg = htmlspecialchars($_GET["msg"]);
					if($section == 'list' && $msg == 'update'){
									$alert = 'success';
									$msgs = 'Le destinataire a été bien modifié.';
								} 
					if($section == 'list' && $msg == 'add'){
									$alert = 'success';
									$msgs = 'Le destinataire a été bien ajouté.';
								} 			
					if($section == 'list' && $msg == 'del'){
									$alert = 'success';
									$msgs = 'Le destinataire a été bien supprimé.';
								} 

								?>
									<?php
								if (isset($msg)){
							?>
								<div class="alert alert-<?php echo $alert; ?> alert-dismissable">
								  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  	<center><?php echo $msgs; ?></center>
								</div>
							<?php
								}}
							?>

						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="portlet box blue">
							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
		                        	 <?php
			                     	if($section=='list'){ echo "Liste des destinataires";}
			                     	if($section=='add'){ echo "Ajouter un destinataire";}

		                        	  ?>
		                        </h4>
		                     </div> 
		                     
		                     <div class="portlet-body form">
	 <?php if($section=='list'){ ?>
		                     <a class="btn green" style="margin-bottom:5px;" href="destinataires.php?section=add">Ajouter <i class="fa fa-plus"></i></a>  

		                     		<table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">N°</th>
			            							<th style="text-align:center;">Nom</th>
			            							<th style="text-align:center;">E-mail</th>
			            							<th style="text-align:center;">Actions</th>
			            							
			  									
			            							
			            						</tr>
<?php 

  $req='SELECT * FROM `destinataires`';
  $res=mysql_query($req);
		
while ($r=mysql_fetch_assoc($res)) { ?>						
						 						

												<tr>
			            							<td><?php echo $r['id']?></td>
			            							<td><?php echo $r['nom']?></td>
			            							<td><?php echo $r['email']?></td>
			            							<td  style="text-align:center;">
			            							<a class="btn red" OnClick="return confirm('Voulez vous vraiment supprimer cet destinataire  ?')" href="destinataires.php?section=list&del=remove&id=<?php echo $r['id']?>"><i class="icon-trash"></i></a>
			            							<a class="btn blue tooltips"  href="editdestinataire.php?section=mod&id=<?php echo $r['id']?>"><i class="icon-pencil"></i></a>
													</td>
			            						</tr>
	<?php } ?>      
		            							</table>

		            					<?php
		                     			if (isset($_GET["del"]) && isset($_GET["id"])){
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			$del = htmlspecialchars($_GET["del"]);
											if($del=='remove'){							
												mysql_query('delete from `destinataires` where id='.$id);
												//header('location : secteur.php?section=list&msg=ok');
												echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; destinataires.php?section=list&msg=del" /> ';
											}
										}
									
		                     	?> 	  
    <?php } if($section=='add'){ ?>

								<form class="form-horizontal" action="codes.php" method="post">
									<input name="section" value="result" type="hidden">
									<input name="destinataire" value="destinataire" type="hidden">
			                     	<div class="control-group">
		                              <label class="control-label">Nom</label>
		                              <div class="controls">
		                                 <input type="text" name="nom" data-original-title="Inserer un nom" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>
								   <div class="control-group">
		                              <label class="control-label">E-mail</label>
		                              <div class="controls">
		                                 <input type="text" name="email" data-original-title="Inserer un E-mail" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>
									<div class="form-actions">
		                              <button class="btn blue" type="submit">Ajouter</button>
		                            
		                           </div>

		                        </form>  

		                     	<?php
		                     	include('config.php');	

								$req11='SELECT * FROM `alerte` WHERE `date`=NOW()';
								$res11=mysql_query($req11); 
								$tab = mysql_fetch_assoc($res11);

								$log='SELECT email FROM `login`';
								$resLog=mysql_query($log); 
								$tabLog=mysql_fetch_assoc($resLog);
								?>

						         </div>
	</div>
			  
		                            </div>

		             <?php }} else{?>

							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />

		             <?php }?> 

		                </div>
		                   
					</div>
		
					
					</div>
				</div>
				<!-- END PAGE CONTENT-->



			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/lightbox/bootstrap-lightbox.min.js"></script>
    

	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();

			//$('#demoLightbox').lightbox(options);
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>