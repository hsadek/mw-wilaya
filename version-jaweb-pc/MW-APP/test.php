<?php
require('fpdf.php');

class PDF extends FPDF
{
// En-tête


// Pied de page
function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro de page
    $this->SetX(150);
    $this->Cell(60,10,'Page '.$this->PageNo().'/{nb}');
    $this->Cell(10,10,'Application Web E-presse');

}
}

// Instanciation de la classe dérivée
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);

$pdf->Output();
?>