<?php ob_start();session_start();
if (isset($_SESSION["username"])) {

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">

    <title>monitoring mobile</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <!-- Modal windows-->
    <link rel="stylesheet" type="text/css" href="modal/css/component.css" />
    <script src="jquery-1.9.0.min.js"></script>
    <script src="modal/js/modernizr.custom.js">
        m=0;
    </script>
   
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<section id="container" >
<?php include_once("header.php"); ?>
<aside>
    <div id="sidebar" class="nav-collapse">

      <?php 
$ul="n";
      include_once("sidebar.php"); ?>
    </div>
</aside>
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        liste des notifications
                   
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
         
                    </header>
                                       <?php  if (isset($_SESSION["modif"])) { ?>
<div class="alert alert-success fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                   <strong>Modification! </strong>Votre Modification a eté Enregistrer.
</div>
<?php } unset($_SESSION["modif"]);?>
                    <div class="panel-body">
    <!--<div class="pull-left" style="margin: 0 20px 20px 0;">
<a href="addarticle.php" class="btn btn-success">ajouter <span class="glyphicon glyphicon-plus"></span></a>
</div>-->
                    
         <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
           <thead>
<!--
                                         `id`, `numero`, `source`, `titre`, `contenu`, `categorie`, `code_payes`, `auteur`, `image`
       
-->
                <tr role="row">
                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="First Name" style="width: 170px;">#</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending" style="width: 166px;">Titre</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending" style="width: 166px;">Message</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending" style="width: 166px;">Action</th>

                </tr>
           </thead>
<?php 
if (isset($_GET["p"])) {
    $p=($_GET["p"]-1)*6;

 }else{
    $p=0;
 }

?>
                                
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                  
<?php 
if (isset($_GET["del"])) {
$id=urldecode($_GET["id"]);
$requete="DELETE FROM notification WHERE id=".$id."";
$resultat=mysql_query($requete);

$_SESSION["modif"]=1;
header("Location:notification.php");
}
/*
notification
`id`, `titre`, `message1`
*/
$requete="SELECT * FROM `notification` limit 3";
$resultat=mysql_query($requete);
$r=mysql_fetch_assoc($resultat);
if(!empty($r['id'])){

 do { ?>
    <tr class="odd">
        <td class="#"><?php echo $r["id"]; ?></td>
        <td class="#"><?php echo $r["titre"]; ?></td>
        <td class="#"><?php echo $r["message1"]; ?></td>
        <td class="#">
            
        <a href="modif.php?post_id=<?php echo $r['id'];?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i></a>
        </td>
    </tr>
   
<?php }while ($r=mysql_fetch_assoc($resultat)); }?> 

<!--
        <td class="#">
<a href="edituser.php?id=<?php echo $r['id']; ?>" class="btn btn-warning"><i class="fa fa-refresh"></i></a>
<a href="utlisateur.php?del=1&id=<?php echo $r["id"]; ?>" class="btn btn-danger"><i class="fa fa-refresh"></i></a>


        </td>-->
      
  

                                </tbody>
                                </table>
                                <!--<div class="row"><div class="col-lg-6"><div class="dataTables_info" id="editable-sample_info">Showing 1 to 5 of 28 entries</div></div><div class="col-lg-6"><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#">← Prev</a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#">Next → </a></li></ul></div></div></div></div>-->
   <!--                  
    <ul class="pagination pull-right">
        <!--<li><a href="#">«</a></li>
<?php 
$req2="SELECT count(*) FROM admins";
$t=mysql_query($req2);
$nbrp=ceil($t/10);
for ($i=0; $i <$nbrp; $i++) { 
?>
<li><a href="utlisateur.php?p=<?php echo $i+1;?>"><?php echo $i+1; ?></a></li>
<?php } ?>


   <li><a href="#">»</a></li>
    </ul>-->


                       </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
            <!-- window modal start-->

        <div class="md-modal md-effect-1" id="modal-1">
            <div class="md-content">

            </div>

        </div>
        <div class="md-overlay"></div><!-- the overlay element -->
        <!-- window modal end-->
        <!-- page end-->
<script>
    $(function() {

        $(" tr td div ").click(function() {

            $.ajax({
                type : 'GET',
                url : 'edit.php?post_id='+m, // in here you should put your query 
               
         
                     // in php you should use $_POST['post_id'] to get this value 
        success : function(data)
           {
              $(".md-content").html(data);  
           }
           
    });
                
            });
    });
    </script>
 
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>
                         

<!-- Placed js at the end of the document so the pages load faster -->

<!-- scripts of modal window start -->
        

        <!-- classie.js by @desandro: https://github.com/desandro/classie -->
        <script src="modal/js/classie.js"></script>
        <script src="modal/js/modalEffects.js"></script>

        <!-- for the blur effect -->
        <!-- by @derSchepp https://github.com/Schepp/CSS-Filters-Polyfill -->
        <script>
            // this is important for IEs
            var polyfilter_scriptpath = '/js/';
        </script>
        <script src="modal/js/cssParser.js"></script>
        <script src="modal/js/css-filters-polyfill.js"></script>

    <!-- scripts of modal window end -->


<!--Core js-->
<script src="js/lib/jquery.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>

<!--common script init for all pages-->
<script src="js/scripts.js"></script>

  
</body>
</html>

<?php }else{header("Location:login.php");} ?>
