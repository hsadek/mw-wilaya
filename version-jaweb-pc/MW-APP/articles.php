<?xml version="1.0" encoding="UTF-8"?>
<document>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AD</first-name>
      <last-name>Andorra</last-name>
      <address>
         <street>Andorre</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AE</first-name>
      <last-name>United Arab Emirates</last-name>
      <address>
         <street>Emirats Arabes Unis</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AF</first-name>
      <last-name>Afghanistan</last-name>
      <address>
         <street>Afghanistan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AG</first-name>
      <last-name>Antigua and Barbuda</last-name>
      <address>
         <street>Antigua-et-Barbuda</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AI</first-name>
      <last-name>Anguilla</last-name>
      <address>
         <street>Anguilla</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AL</first-name>
      <last-name>Albania</last-name>
      <address>
         <street>Albanie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AM</first-name>
      <last-name>Armenia</last-name>
      <address>
         <street>Armenie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AN</first-name>
      <last-name />
      <address>
         <street>Antilles neerlandaises</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AO</first-name>
      <last-name>Angola</last-name>
      <address>
         <street>Angola</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AP</first-name>
      <last-name>African Regional Intellectual Property Organization (ARIPO)(1)</last-name>
      <address>
         <street>ARIPO</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AR</first-name>
      <last-name>Argentina</last-name>
      <address>
         <street>Argentine</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AT</first-name>
      <last-name>Austria</last-name>
      <address>
         <street>Autriche</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AU</first-name>
      <last-name>Australia</last-name>
      <address>
         <street>Australie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AW</first-name>
      <last-name>Aruba</last-name>
      <address>
         <street>Aruba</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>AZ</first-name>
      <last-name>Azerbaijan</last-name>
      <address>
         <street>Azerbaidjan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BA</first-name>
      <last-name>Bosnia and Herzegovina</last-name>
      <address>
         <street>Bosnie-Herzegovine</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BB</first-name>
      <last-name>Barbados</last-name>
      <address>
         <street>Barbade</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BD</first-name>
      <last-name>Bangladesh</last-name>
      <address>
         <street>Bangladesh</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BE</first-name>
      <last-name>Belgium</last-name>
      <address>
         <street>Belgique</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BF</first-name>
      <last-name>Burkina Faso</last-name>
      <address>
         <street>Burkina Faso</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BG</first-name>
      <last-name>Bulgaria</last-name>
      <address>
         <street>Bulgarie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BH</first-name>
      <last-name>Bahrain</last-name>
      <address>
         <street>Bahrein</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BI</first-name>
      <last-name>Burundi</last-name>
      <address>
         <street>Burundi</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BJ</first-name>
      <last-name>Benin</last-name>
      <address>
         <street>Benin</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BM</first-name>
      <last-name>Bermuda</last-name>
      <address>
         <street>Bermudes</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BN</first-name>
      <last-name>Brunei Darussalam</last-name>
      <address>
         <street>Brunei Darussalam</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BO</first-name>
      <last-name>Bolivia</last-name>
      <address>
         <street>Plurinational State of</street>
         <city>Bolivie</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BR</first-name>
      <last-name>Brazil</last-name>
      <address>
         <street>Bresil</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BS</first-name>
      <last-name>Bahamas</last-name>
      <address>
         <street>Bahamas</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BT</first-name>
      <last-name>Bhutan</last-name>
      <address>
         <street>Bhoutan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BV</first-name>
      <last-name>Bouvet Island</last-name>
      <address>
         <street>le Bouvet</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BW</first-name>
      <last-name>Botswana</last-name>
      <address>
         <street>Botswana</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BX</first-name>
      <last-name>Benelux Office for Intellectual Property (BOIP)(2)</last-name>
      <address>
         <street>BENELUX</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BY</first-name>
      <last-name>Belarus</last-name>
      <address>
         <street>Belarus</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BZ</first-name>
      <last-name>Belize</last-name>
      <address>
         <street>Belize</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CA</first-name>
      <last-name>Canada</last-name>
      <address>
         <street>Canada</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CF</first-name>
      <last-name>Central African Republic</last-name>
      <address>
         <street>Republique centrafricaine</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CD</first-name>
      <last-name>Democratic Republic of the Congo</last-name>
      <address>
         <street>Republique democratique du Congo</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CG</first-name>
      <last-name>Congo</last-name>
      <address>
         <street>Congo</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CH</first-name>
      <last-name>Switzerland</last-name>
      <address>
         <street>Suisse</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CI</first-name>
      <last-name>Cote dIvoire</last-name>
      <address>
         <street>Cote dIvoire</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CK</first-name>
      <last-name>Cook Islands</last-name>
      <address>
         <street>les Cook</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CL</first-name>
      <last-name>Chile</last-name>
      <address>
         <street>Chili</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CM</first-name>
      <last-name>Cameroon</last-name>
      <address>
         <street>Cameroun</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CN</first-name>
      <last-name>China</last-name>
      <address>
         <street>Chine</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CO</first-name>
      <last-name>Colombia</last-name>
      <address>
         <street>Colombie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CR</first-name>
      <last-name>Costa Rica</last-name>
      <address>
         <street>Costa Rica</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CU</first-name>
      <last-name>Cuba</last-name>
      <address>
         <street>Cuba</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CV</first-name>
      <last-name>Cabo Verde</last-name>
      <address>
         <street>Cap-Vert</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CY</first-name>
      <last-name>Cyprus</last-name>
      <address>
         <street>Chypre</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CZ</first-name>
      <last-name>Czech Republic</last-name>
      <address>
         <street>Republique tcheque</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>DT</first-name>
      <last-name>Germany</last-name>
      <address>
         <street>Allemagne</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>DJ</first-name>
      <last-name>Djibouti</last-name>
      <address>
         <street>Djibouti</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>DK</first-name>
      <last-name>Denmark</last-name>
      <address>
         <street>Danemark</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>DM</first-name>
      <last-name>Dominica</last-name>
      <address>
         <street>Dominique</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>DO</first-name>
      <last-name>Dominican Republic</last-name>
      <address>
         <street>Republique dominicaine</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>DZ</first-name>
      <last-name>Algeria</last-name>
      <address>
         <street>Algerie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>EA</first-name>
      <last-name>Eurasian Patent Organization (EAPO)(1)</last-name>
      <address>
         <street>Organisation eurasienne des brevets (OEAB)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>EC</first-name>
      <last-name>Ecuador</last-name>
      <address>
         <street>quateur</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>EE</first-name>
      <last-name>Estonia</last-name>
      <address>
         <street>Estonie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>EG</first-name>
      <last-name>Egypt</last-name>
      <address>
         <street>gypte</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>EH</first-name>
      <last-name>Western Sahara(5)</last-name>
      <address>
         <street>Sahara occidental</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>EM</first-name>
      <last-name>Office for Harmonization in the Internal Market (Trademarks and Designs) (OHIM)</last-name>
      <address>
         <street>Union Europeenne</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>EP</first-name>
      <last-name>European Patent Office (EPO)(1)</last-name>
      <address>
         <street>Office europeen des brevets (OEB)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ER</first-name>
      <last-name>Eritrea</last-name>
      <address>
         <street>rythree</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ES</first-name>
      <last-name>Spain</last-name>
      <address>
         <street>Espagne</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ET</first-name>
      <last-name>Ethiopia</last-name>
      <address>
         <street>thiopie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>FI</first-name>
      <last-name>Finland</last-name>
      <address>
         <street>Finlande</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>FJ</first-name>
      <last-name>Fiji</last-name>
      <address>
         <street>Fidji</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>FK</first-name>
      <last-name>Falkland Islands (Malvinas)</last-name>
      <address>
         <street>les Falkland (Malvinas)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>FO</first-name>
      <last-name>Faroe Islands</last-name>
      <address>
         <street>les Feroe</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>FR</first-name>
      <last-name>France</last-name>
      <address>
         <street>France</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GA</first-name>
      <last-name>Gabon</last-name>
      <address>
         <street>Gabon</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GC</first-name>
      <last-name>Patent Office of the Cooperation Council for the Arab States of the Gulf (GCC)</last-name>
      <address>
         <street>Office des brevets du Conseil de cooperation des tats arabes du Golfe (CCG)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GD</first-name>
      <last-name>Grenada</last-name>
      <address>
         <street>Grenade</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GE</first-name>
      <last-name>Georgia</last-name>
      <address>
         <street>Georgie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GG</first-name>
      <last-name>Guernsey</last-name>
      <address>
         <street>Guernesey</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GH</first-name>
      <last-name>Ghana</last-name>
      <address>
         <street>Ghana</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GI</first-name>
      <last-name>Gibraltar</last-name>
      <address>
         <street>Gibraltar</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GL</first-name>
      <last-name>Greenland</last-name>
      <address>
         <street>Groenland</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GM</first-name>
      <last-name>Gambia</last-name>
      <address>
         <street>Gambie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GN</first-name>
      <last-name>Guinea</last-name>
      <address>
         <street>Guinee</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GQ</first-name>
      <last-name>Equatorial Guinea</last-name>
      <address>
         <street>Guinee equatoriale</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GR</first-name>
      <last-name>Greece</last-name>
      <address>
         <street>Grece</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GS</first-name>
      <last-name>South Georgia and the South Sandwich Islands</last-name>
      <address>
         <street>Georgie du Sud et les îles Sandwich du Sud</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GT</first-name>
      <last-name>Guatemala</last-name>
      <address>
         <street>Guatemala</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GW</first-name>
      <last-name>Guinea-Bissau</last-name>
      <address>
         <street>Guinee-Bissau</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GY</first-name>
      <last-name>Guyana</last-name>
      <address>
         <street>Guyana</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>HN</first-name>
      <last-name>Honduras</last-name>
      <address>
         <street>Honduras</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>HR</first-name>
      <last-name>Croatia</last-name>
      <address>
         <street>Croatie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>HT</first-name>
      <last-name>Haiti</last-name>
      <address>
         <street>Haiti</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>HU</first-name>
      <last-name>Hungary</last-name>
      <address>
         <street>Hongrie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IB</first-name>
      <last-name>International Bureau of the World Intellectual Property Organization (WIPO)(4)</last-name>
      <address>
         <street>Bureau international de lOrganisation Mondiale de la Propriete Intellectuelle (OMPI)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ID</first-name>
      <last-name>Indonesia</last-name>
      <address>
         <street>Indonesie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IE</first-name>
      <last-name>Ireland</last-name>
      <address>
         <street>Irlande</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IL</first-name>
      <last-name>Israel</last-name>
      <address>
         <street>Israel</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IM</first-name>
      <last-name>Isle of Man</last-name>
      <address>
         <street>le de Man</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IN</first-name>
      <last-name>India</last-name>
      <address>
         <street>Inde</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IQ</first-name>
      <last-name>Iraq</last-name>
      <address>
         <street>Iraq</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IR</first-name>
      <last-name>Iran (Islamic Republic of)</last-name>
      <address>
         <street>Iran</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IS</first-name>
      <last-name>Iceland</last-name>
      <address>
         <street>Islande</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>IT</first-name>
      <last-name>Italy</last-name>
      <address>
         <street>Italie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>JE</first-name>
      <last-name>Jersey</last-name>
      <address>
         <street>Jersey</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>JM</first-name>
      <last-name>Jamaica</last-name>
      <address>
         <street>Jamaique</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>JO</first-name>
      <last-name>Jordan</last-name>
      <address>
         <street>Jordanie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>JP</first-name>
      <last-name>Japan</last-name>
      <address>
         <street>Japon</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KE</first-name>
      <last-name>Kenya</last-name>
      <address>
         <street>Kenya</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KG</first-name>
      <last-name>Kyrgyzstan</last-name>
      <address>
         <street>Kirghizistan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KH</first-name>
      <last-name>Cambodia</last-name>
      <address>
         <street>Cambodge</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KI</first-name>
      <last-name>Kiribati</last-name>
      <address>
         <street>Kiribati</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KM</first-name>
      <last-name>Comoros</last-name>
      <address>
         <street>Comores</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KN</first-name>
      <last-name>Saint Kitts and Nevis</last-name>
      <address>
         <street>Saint-Kitts-et-Nevis</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KP</first-name>
      <last-name>Democratic Peoples Republic of Korea</last-name>
      <address>
         <street>Republique populaire democratique de Coree</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KR</first-name>
      <last-name>Republic of Korea</last-name>
      <address>
         <street>Republique de Coree</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KW</first-name>
      <last-name>Kuwait</last-name>
      <address>
         <street>Koweit</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KY</first-name>
      <last-name>Cayman Islands</last-name>
      <address>
         <street>les Caimans</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>KZ</first-name>
      <last-name>Kazakhstan</last-name>
      <address>
         <street>Kazakhstan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LA</first-name>
      <last-name>Lao Peoples Democratic Republic</last-name>
      <address>
         <street>Republique democratique populaire lao</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LB</first-name>
      <last-name>Lebanon</last-name>
      <address>
         <street>Liban</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LC</first-name>
      <last-name>Saint Lucia</last-name>
      <address>
         <street>Sainte-Lucie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LI</first-name>
      <last-name>Liechtenstein</last-name>
      <address>
         <street>Liechtenstein</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LK</first-name>
      <last-name>Sri Lanka</last-name>
      <address>
         <street>Sri Lanka</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LR</first-name>
      <last-name>Liberia</last-name>
      <address>
         <street>Liberia</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LS</first-name>
      <last-name>Lesotho</last-name>
      <address>
         <street>Lesotho</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LT</first-name>
      <last-name>Lithuania</last-name>
      <address>
         <street>Lituanie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LU</first-name>
      <last-name>Luxembourg</last-name>
      <address>
         <street>Luxembourg</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LV</first-name>
      <last-name>Latvia</last-name>
      <address>
         <street>Lettonie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>LY</first-name>
      <last-name>Libya</last-name>
      <address>
         <street>Lybie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MA</first-name>
      <last-name>Morocco</last-name>
      <address>
         <street>Maroc</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MC</first-name>
      <last-name>Monaco</last-name>
      <address>
         <street>Monaco</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MD</first-name>
      <last-name>Republic of Moldova</last-name>
      <address>
         <street>Republique de Moldova</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ME</first-name>
      <last-name>Montenegro</last-name>
      <address>
         <street>Montenegro</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MG</first-name>
      <last-name>Madagascar</last-name>
      <address>
         <street>Madagascar</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MK</first-name>
      <last-name>The former Yugoslav Republic of Macedonia</last-name>
      <address>
         <street>Ex-Republique yougoslave de Macedoine</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ML</first-name>
      <last-name>Mali</last-name>
      <address>
         <street>Mali</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MM</first-name>
      <last-name>Myanmar</last-name>
      <address>
         <street>Myanmar</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MN</first-name>
      <last-name>Mongolia</last-name>
      <address>
         <street>Mongolie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MO</first-name>
      <last-name>Macao</last-name>
      <address>
         <street>Macao</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MP</first-name>
      <last-name>Northern Mariana Islands</last-name>
      <address>
         <street>les Mariannes du Nord</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MR</first-name>
      <last-name>Mauritania</last-name>
      <address>
         <street>Mauritanie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MS</first-name>
      <last-name>Montserrat</last-name>
      <address>
         <street>Montserrat</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MT</first-name>
      <last-name>Malta</last-name>
      <address>
         <street>Malte</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MU</first-name>
      <last-name>Mauritius</last-name>
      <address>
         <street>Maurice</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MV</first-name>
      <last-name>Maldives</last-name>
      <address>
         <street>Maldives</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MW</first-name>
      <last-name>Malawi</last-name>
      <address>
         <street>Malawi</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MX</first-name>
      <last-name>Mexico</last-name>
      <address>
         <street>Mexique</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MY</first-name>
      <last-name>Malaysia</last-name>
      <address>
         <street>Malaisie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>MZ</first-name>
      <last-name>Mozambique</last-name>
      <address>
         <street>Mozambique</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NA</first-name>
      <last-name>Namibia</last-name>
      <address>
         <street>Namibie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NE</first-name>
      <last-name>Niger</last-name>
      <address>
         <street>Niger</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NG</first-name>
      <last-name>Nigeria</last-name>
      <address>
         <street>Nigeria</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NI</first-name>
      <last-name>Nicaragua</last-name>
      <address>
         <street>Nicaragua</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NL</first-name>
      <last-name>Netherlands</last-name>
      <address>
         <street>Pays-Bas</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NO</first-name>
      <last-name>Norway</last-name>
      <address>
         <street>Norvege</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NP</first-name>
      <last-name>Nepal</last-name>
      <address>
         <street>Nepal</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NR</first-name>
      <last-name>Nauru</last-name>
      <address>
         <street>Nauru</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>NZ</first-name>
      <last-name>New Zealand</last-name>
      <address>
         <street>Nouvelle-Zelande</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>OA</first-name>
      <last-name>African Intellectual Property Organization (OAPI)(1)</last-name>
      <address>
         <street>Organisation africaine de la propriete intellectuelle (OAPI)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>OM</first-name>
      <last-name>Oman</last-name>
      <address>
         <street>Oman</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PA</first-name>
      <last-name>Panama</last-name>
      <address>
         <street>Panama</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PE</first-name>
      <last-name>Peru</last-name>
      <address>
         <street>Perou</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PG</first-name>
      <last-name>Papua New Guinea</last-name>
      <address>
         <street>Papouasie-Nouvelle-Guinee</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PH</first-name>
      <last-name>Philippines</last-name>
      <address>
         <street>Philippines</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PK</first-name>
      <last-name>Pakistan</last-name>
      <address>
         <street>Pakistan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PL</first-name>
      <last-name>Poland</last-name>
      <address>
         <street>Pologne</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PT</first-name>
      <last-name>Portugal</last-name>
      <address>
         <street>Portugal</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PW</first-name>
      <last-name>Palau</last-name>
      <address>
         <street>Palaos</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>PY</first-name>
      <last-name>Paraguay</last-name>
      <address>
         <street>Paraguay</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>QA</first-name>
      <last-name>Qatar</last-name>
      <address>
         <street>Qatar</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>QZ</first-name>
      <last-name>Community Plant Variety Office (European Union) (CPVO)</last-name>
      <address>
         <street>Office communautaire des varietes vegetales(Communaute europeenne) (OCVV)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>RO</first-name>
      <last-name>Romania</last-name>
      <address>
         <street>Roumanie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>RS</first-name>
      <last-name>Serbia</last-name>
      <address>
         <street>Serbie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>RU</first-name>
      <last-name>Russian Federation</last-name>
      <address>
         <street>Federation de Russie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>RW</first-name>
      <last-name>Rwanda</last-name>
      <address>
         <street>Rwanda</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SA</first-name>
      <last-name>Saudi Arabia</last-name>
      <address>
         <street>Arabie Saoudite</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SB</first-name>
      <last-name>Solomon Islands</last-name>
      <address>
         <street>les Salomon</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SC</first-name>
      <last-name>Seychelles</last-name>
      <address>
         <street>Seychelles</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SD</first-name>
      <last-name>Sudan</last-name>
      <address>
         <street>Soudan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SE</first-name>
      <last-name>Sweden</last-name>
      <address>
         <street>Suede</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SG</first-name>
      <last-name>Singapore</last-name>
      <address>
         <street>Singapour</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SH</first-name>
      <last-name>Saint Helena</last-name>
      <address>
         <street>Ascension and Tristan da Cunha</street>
         <city>Sainte-Helene</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SI</first-name>
      <last-name>Slovenia</last-name>
      <address>
         <street>Slovenie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SK</first-name>
      <last-name>Slovakia</last-name>
      <address>
         <street>Slovaquie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SL</first-name>
      <last-name>Sierra Leone</last-name>
      <address>
         <street>Sierra Leone</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SM</first-name>
      <last-name>San Marino</last-name>
      <address>
         <street>Saint-Marin</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SN</first-name>
      <last-name>Senegal</last-name>
      <address>
         <street>Senegal</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SO</first-name>
      <last-name>Somalia</last-name>
      <address>
         <street>Somalie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SR</first-name>
      <last-name>Suriname</last-name>
      <address>
         <street>Suriname</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ST</first-name>
      <last-name>Sao Tome and Principe</last-name>
      <address>
         <street>Sao Tome-et-Principe</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SV</first-name>
      <last-name>El Salvador</last-name>
      <address>
         <street>El Salvador</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SY</first-name>
      <last-name>Syrian Arab Republic</last-name>
      <address>
         <street>SYRIE</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SZ</first-name>
      <last-name>Swaziland</last-name>
      <address>
         <street>Swaziland</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TC</first-name>
      <last-name>Turks and Caicos Islands</last-name>
      <address>
         <street>les Turks et Caiques</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TD</first-name>
      <last-name>Chad</last-name>
      <address>
         <street>Tchad</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TG</first-name>
      <last-name>Togo</last-name>
      <address>
         <street>Togo</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TH</first-name>
      <last-name>Thailand</last-name>
      <address>
         <street>Thailande</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TJ</first-name>
      <last-name>Tajikistan</last-name>
      <address>
         <street>Tadjikistan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TL</first-name>
      <last-name>Timor–Leste</last-name>
      <address>
         <street>Timor-Leste</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TM</first-name>
      <last-name>Turkmenistan</last-name>
      <address>
         <street>Turkmenistan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TN</first-name>
      <last-name>Tunisia</last-name>
      <address>
         <street>Tunisie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TO</first-name>
      <last-name>Tonga</last-name>
      <address>
         <street>Tonga</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TR</first-name>
      <last-name>Turkey</last-name>
      <address>
         <street>Turquie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TT</first-name>
      <last-name>Trinidad and Tobago</last-name>
      <address>
         <street>Trinite-et-Tobago</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TV</first-name>
      <last-name>Tuvalu</last-name>
      <address>
         <street>Tuvalu</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TW</first-name>
      <last-name>Taiwan</last-name>
      <address>
         <street>Province of China</street>
         <city>Taiwan</city>
         <state>Province de Chine</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>TZ</first-name>
      <last-name>United Republic of Tanzania</last-name>
      <address>
         <street>Republique-Unie de Tanzanie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>UA</first-name>
      <last-name>Ukraine</last-name>
      <address>
         <street>Ukraine</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>UG</first-name>
      <last-name>Uganda</last-name>
      <address>
         <street>Ouganda</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>US</first-name>
      <last-name>United States of America</last-name>
      <address>
         <street>tats-Unis dAmerique</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>UY</first-name>
      <last-name>Uruguay</last-name>
      <address>
         <street>Uruguay</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>UZ</first-name>
      <last-name>Uzbekistan</last-name>
      <address>
         <street>Ouzbekistan</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>VA</first-name>
      <last-name>Holy See</last-name>
      <address>
         <street>Saint-Siege</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>VC</first-name>
      <last-name>Saint Vincent and the Grenadines</last-name>
      <address>
         <street>Saint-Vincent-et-les Grenadines</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>VE</first-name>
      <last-name>Venezuela</last-name>
      <address>
         <street>Bolivarian Republic of</street>
         <city>Venezuela</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>VG</first-name>
      <last-name>Virgin Islands (British)</last-name>
      <address>
         <street>ILES VIERGES BRITANNIQUES</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>VU</first-name>
      <last-name>Vanuatu</last-name>
      <address>
         <street>Vanuatu</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>WO</first-name>
      <last-name>World Intellectual Property Organization (WIPO) (International Bureau of)(4)</last-name>
      <address>
         <street>Organisation Mondiale de la Propriete Intellectuelle (OMPI) (Bureau international de l)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>WS</first-name>
      <last-name>Samoa</last-name>
      <address>
         <street>Samoa</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>XN</first-name>
      <last-name>Nordic Patent Institute (NPI)</last-name>
      <address>
         <street>Institut nordique des brevets (INB)</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>YE</first-name>
      <last-name>Yemen</last-name>
      <address>
         <street>Yemen</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ZA</first-name>
      <last-name>South Africa</last-name>
      <address>
         <street>Afrique du Sud</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ZM</first-name>
      <last-name>Zambia</last-name>
      <address>
         <street>Zambie</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>ZW</first-name>
      <last-name>Zimbabwe</last-name>
      <address>
         <street>Zimbabwe</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>GB</first-name>
      <last-name>United Kingdom</last-name>
      <address>
         <street>GRANDE BRETAGNE</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>HK</first-name>
      <last-name>The Hong Kong Special Administrative Region of the Peoples Republic of China</last-name>
      <address>
         <street>HONG-KONG</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>VN</first-name>
      <last-name>Viet Nam</last-name>
      <address>
         <street>VIETNAM</street>
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>BQ</first-name>
      <last-name>Bonaire</last-name>
      <address>
         <street>Sint Eustatius and Saba</street>
         <city />
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>CW</first-name>
      <last-name>Curacao</last-name>
      <address>
         <street />
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SS</first-name>
      <last-name>South Sudan</last-name>
      <address>
         <street />
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
   // Example of a template. The template is used for EACH line of the CSV file
// and should NOT contain a document declaration:
   <person id="">
      <first-name>SX</first-name>
      <last-name>Sint Maarten (Dutch part)</last-name>
      <address>
         <street />
         <city>##5##</city>
         <state>##6##</state>
         <country>##7##</country>
         <zip>##8##</zip>
      </address>
   </person>
</document>