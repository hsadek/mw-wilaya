-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le: Mer 03 Septembre 2014 à 03:11
-- Version du serveur: 5.5.32
-- Version de PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `eentrepr_wm`
--
CREATE DATABASE IF NOT EXISTS `inovteam_mw` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `inovteam_mw`;

-- --------------------------------------------------------

--
-- Structure de la table `alerte`
--

CREATE TABLE IF NOT EXISTS `alerte` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mot_cle` varchar(255) NOT NULL,
  `site` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `alerte`
--

INSERT INTO `alerte` (`id`, `mot_cle`, `site`, `date`) VALUES
(5, 'hes', '2', '2014-08-30');

-- --------------------------------------------------------

--
-- Structure de la table `archive`
--

CREATE TABLE IF NOT EXISTS `archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cle` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `linkid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `titrearticle` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `local_url` varchar(255) NOT NULL,
  `prefecteur_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Contenu de la table `archive`
--

INSERT INTO `archive` (`id`, `cle`, `datetime`, `linkid`, `sid`, `titrearticle`, `description`, `url`, `local_url`, `prefecteur_id`) VALUES
(29, 'Benkirane', '2014-01-28 16:56:15', 2, 2, 'Hespress - Ù‡Ø³Ø¨Ø±ÙŠØ³ Ø¬Ø±ÙŠØ¯Ø© Ø¥Ù„ÙƒØªØ±ÙˆÙ†ÙŠØ© Ù…ØºØ±Ø¨ÙŠØ©', 'Ø£ÙˆÙ„ Ø¬Ø±ÙŠØ¯Ø© Ø¥Ù„ÙƒØªØ±ÙˆÙ†ÙŠØ© Ù…ØºØ±Ø¨ÙŠØ© ØªØ¬Ø¯Ø¯ Ø¹Ù„Ù‰ Ù…Ø¯Ø§Ø± Ø§Ù„Ø³Ø§Ø¹Ø© - Ø§Ù„Ù…ØºØ±Ø¨ - - Hespress officiel - est \r\nune source centrale d''informations et de nouvelles sur le Maroc.', 'http://www.hespress.com/', '', 1),
(30, 'Benkirane', '2014-01-28 16:56:25', 2, 4, 'Ø±ÙˆÙŠØªØ±Ø²: Ù…Ù„Ùƒ Ø§Ù„Ù…ØºØ±Ø¨ ÙŠØ¹ÙŠÙ† Ø³ÙŠØ§Ø³ÙŠØ§ ÙŠØ¨ØºØ¶Ù‡ Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠÙˆÙ† Ù…Ø³ØªØ´Ø§Ø±Ø§ Ù„Ù‡', '8 ÙƒØ§Ù†ÙˆÙ† Ø§Ù„Ø£ÙˆÙ„ (Ø¯ÙŠØ³Ù…Ø¨Ø±) 2011 &lt;b&gt;...&lt;/b&gt; politiquement il est plus fort que &lt;b&gt;benkirane&lt;/b&gt;,. ØªØ¹Ù„ÙŠÙ‚ ØºÙŠØ± Ù„Ø§Ø¦Ù‚ Ø£Ø¹Ù„Ù‰ Ø§Ù„ØµÙØ­Ø© Ù…Ù‚Ø¨ÙˆÙ„ \r\nÙ…Ø±ÙÙˆØ¶. 0. 5 - amine Ø§Ù„Ø®Ù…ÙŠØ³ 08 Ø¯Ø¬Ù†Ø¨Ø± 2011 - 01:00. correctionÂ &lt;b&gt;...&lt;/b&gt;', 'http://www.hespress.com/politique/42852.html', '', 3),
(31, 'Benkirane', '2014-01-28 16:56:37', 2, 8, 'Ø³Ø¨ ÙˆØ´ØªÙ… ÙÙŠ Ù†Ø¯ÙˆØ© Ø³ÙŠØ§Ø³ÙŠØ© - Ù‡Ø³Ø¨Ø±ÙŠØ³', '5 ØªØ´Ø±ÙŠÙ† Ø§Ù„Ø«Ø§Ù†ÙŠ (Ù†ÙˆÙÙ…Ø¨Ø±) 2011 &lt;b&gt;...&lt;/b&gt; VOTEZ POUR CE PARTI PJD BON COURAGE Mr &lt;b&gt;BENKIRANE&lt;/b&gt; &lt;b&gt;...&lt;/b&gt; tout \r\nsimplement &lt;b&gt;benkirane&lt;/b&gt; un homme franc et on a vraiment besoin desÂ &lt;b&gt;...&lt;/b&gt;', 'http://www.hespress.com/videos/40722.html', '', 2),
(36, 'Le roi', '2014-02-01 21:34:11', 2, 1, 'ÙÙÙˆØ¶ÙŽÙ‰ Ù…Ø¬Ù„Ø³ Ø§Ù„Ù…Ø³ØªØ´Ø§Ø±ÙŠÙ† - Ù‡Ø³Ø¨Ø±ÙŠØ³', '7 ÙƒØ§Ù†ÙˆÙ† Ø§Ù„Ø«Ø§Ù†ÙŠ (ÙŠÙ†Ø§ÙŠØ±) 2014 &lt;b&gt;...&lt;/b&gt; Le dÃ©sordre de ces prÃ©tendus reprÃ©sentant du peuple circule dans leurs &lt;b&gt;.....&lt;/b&gt; nous \r\nprions Sa MajestÃ© &lt;b&gt;Le Roi&lt;/b&gt; que Dieu l&quot;assiste d&quot;intervenir pourÂ &lt;b&gt;...&lt;/b&gt;', 'http://www.hespress.com/videos/105121.html', '', 1),
(37, 'sadek', '2014-02-06 21:13:33', 3, 1, 'Ø£ÙƒØ§Ø¯ÙŠØ± : Ø´Ø¨Ø§Ø· ÙŠÙ†Ø´Ø± ØºØ³ÙŠÙ„ Ø¨Ù† ÙƒÙŠØ±Ø§Ù† ÙˆÙŠØµÙ Ø¨Ù† Ø¹Ø¨Ø¯ Ø§Ù„Ù„Ù‡ Ø¨ &lt;b&gt;...&lt;/b&gt; - Ù‡Ø¨Ø© Ø¨Ø±ÙŠØ³', '21 Ø£ÙŠØ§Ø± (Ù…Ø§ÙŠÙˆ) 2013 &lt;b&gt;...&lt;/b&gt; 20mowatin &lt;b&gt;sadek&lt;/b&gt;May 21-2013 08:09. Ø´Ø¨Ø§Ø· Ø£ÙƒØ¨Ø± ÙƒØ¯Ø§Ø¨ Ùˆ Ù…Ù†Ø§ÙÙ‚ Ø¹Ù„Ù‰ Ø§Ù„Ø§Ø¡ Ø·Ù„Ø§Ù‚. Ø§Ù†Ù‡ \r\nØ£Ø³ÙˆØ¡ Ø£Ù…ÙŠÙ† Ø¹Ø§Ù… Ù„Ø­Ø²Ø¨ Ø¹Ø±ÙÙ‡ Ø§Ù„Ù…ØºØ±Ø¨ ÙŠØ³Ø¹Ù‰ Ù„ØªØ®Ø±ÙŠØ¨ Ù„Ø§ ÙŠØ¹Ù‚Ù„ Ø§Ù† ÙŠØªØ²Ø¹Ù…Â &lt;b&gt;...&lt;/b&gt;', 'http://static.hibapress.com/details-3562.html', '', 1),
(38, 'casablanca', '2014-03-20 10:08:00', 2, 1, 'Ø§Ù„Ø£Ù…Ù† ÙŠØ¹Ù†Ù ÙˆÙŠØ¹ØªÙ‚Ù„ Ù…Ø­ØªØ¬ÙŠÙ† ÙÙŠ Ø§Ù„Ø¯Ø§Ø± Ø§Ù„Ø¨ÙŠØ¶Ø§Ø¡ - Ù‡Ø³Ø¨Ø±ÙŠØ³', '14 Ø¢Ø°Ø§Ø± (Ù…Ø§Ø±Ø³) 2011 &lt;b&gt;...&lt;/b&gt; l''Alliance royaliste marocaine : CELLULE DE &lt;b&gt;CASABLANCA&lt;/b&gt; En ce moment. LA \r\nCELLULE DE &lt;b&gt;CASABLANCA&lt;/b&gt; EST EN COURS DE CRÃ‰ATIONÂ ...', 'http://www.hespress.com/politique/29029.html', '', 1),
(39, 'casablanca', '2014-03-20 10:08:41', 2, 2, 'Cadastre des autorisations TPV Page 1 de 399', '27 avr. 2006 &lt;b&gt;...&lt;/b&gt; &lt;b&gt;Casablanca&lt;/b&gt; - Beni Mellal et retour par Ben Ahmed - Kouribga - Oued. Zem - \r\nBoujad Kasbah Tadla. Lundi : Boujaad - &lt;b&gt;Casablanca&lt;/b&gt;. &lt;b&gt;Casablanca&lt;/b&gt;.', 'http://s1.hespress.com/files/listemtpnet.pdf', '', 2),
(40, 'ÙƒØ§Ø²Ø§Ø¨Ù„Ø§Ù†ÙƒØ§', '2014-03-25 11:05:59', 3, 1, 'Ø§Ù„Ø±Ø¹Ø¨ ÙŠØ±Ø®ÙŠ Ø³ØªØ§Ø±Ù‡ Ø¹Ù„Ù‰ ÙƒÙ„ Ù…Ù† Ø§Ø±Ø§Ø¯ Ø§Ù„Ù…Ø¬Ø§Ø²ÙØ© Ø¨Ø§Ù„ØªØ­Ø¯Ø« Ø¹Ù† &lt;b&gt;...&lt;/b&gt; - Hibapress', 'ÙˆÙÙŠ Ø­ÙŠÙ† Ø£Ù† Ø§Ù„Ù…Ø«ÙŠØ± Ù„Ù„Ø¬Ø¯Ù„ , ÙˆÙ‡Ùˆ Ø£Ù† Ø§Ù„Ø±Ø®ØµØªÙŠÙ† , Ø£ÙŠ &lt;b&gt;ÙƒØ§Ø²Ø§Ø¨Ù„Ø§Ù†ÙƒØ§&lt;/b&gt; 1Ùˆ2 ÙˆØ£Ø³ÙÙŠ Ø£ÙˆÙØ´ÙˆØ± \r\nØªØ­ØªØ¶Ù†Ø§Ù† Ø³Ø±Ø§ Ù†ÙØ·ÙŠØ§ Ø¢Ø®Ø±, ÙˆÙ‚Ø¯ Ø£Ø®ÙÙŠØªÙ‡ Ù„Ø³Ù†ÙŠÙ† Ø·ÙˆÙŠÙ„Ø©, Ù„ÙƒÙ† Ø¨Ø¹Ø¯ Ù…Ù†Ø­ Ù‡Ø§ØªÙŠÙ† Ø§Ù„Ø±Ø®ØµØªÙŠÙ† \r\nÙŠÙˆÙ… 25Â &lt;b&gt;...&lt;/b&gt;', 'http://hibapress.com/contact/details-3010.html', '', 0),
(41, 'sidi bernoussi', '2014-05-30 16:10:30', 3, 4, 'Lydec investit plus de 3,1 MMDH dans le Grand Casablanca en 2014', '11 mars 2014 &lt;b&gt;...&lt;/b&gt; Le systÃ¨me est constituÃ© de deux intercepteurs cÃ´tiers prÃ©vus pour rassembler \r\nen un point, au niveau de &lt;b&gt;Sidi Bernoussi&lt;/b&gt;, les 9 rejets d''eauxÂ ...', 'http://hibapress.com/old-site/fr-/details-2923.html', '', 1),
(42, 'CASABLANCA', '2014-05-30 20:33:41', 8, 0, 'Â«&lt;b&gt;Casablanca&lt;/b&gt; Finance CityÂ» : Faire du Maroc une terre d''accueil d &lt;b&gt;...&lt;/b&gt;', '16 juil. 2013 &lt;b&gt;...&lt;/b&gt; Casa Finance City (CFC) sera un vecteur d''inclusion financiÃ¨re et d''\r\nÃ©largissement de la bancarisation et de rayonnement de l''Ã©conomieÂ ...', 'http://www.lematin.ma/express/Casablanca-Finance-City-_Faire-du-Maroc-une-terre-d-accueil-d-investissements/185194.html', '', 0),
(43, 'carrieres centrales', '2014-06-09 12:26:31', 2, 0, 'Ø¹Ù† Ø§Ø­ØªØ¬Ø§Ø¬Ø§Øª ÙƒØ§Ø±ÙŠØ§Ù† Ø³Ù†Ø·Ø±Ø§Ù„ - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° 3 ÙŠÙˆÙ… &lt;b&gt;...&lt;/b&gt; Chers compatriotes , karian central qui avait fait naitre la rÃ©sitance de laquelle .... \r\nFeu med 5 a ete surnomme le roi des &lt;b&gt;carrieres centrales&lt;/b&gt; parÂ ...', 'http://www.hespress.com/videos/225721.html', '', 0),
(44, 'carrieres centrales', '2014-06-09 12:29:58', 6, 11, 'DÃ©molition des baraques Ã  â€œ&lt;b&gt;CarriÃ¨res Centrales&lt;/b&gt;â€: La grand-mÃ¨re de &lt;b&gt;...&lt;/b&gt;', 'il y a 2Â jours &lt;b&gt;...&lt;/b&gt; Une femme Ã¢gÃ©e de 85 ans a Ã©tÃ© tuÃ©e vendredi Ã  Casablanca, lors d''un accident \r\nde la circulation survenu au niveau du boulevard IbnÂ ...', 'http://m.aufaitmaroc.com/maroc/societe/2014/6/6/la-grand-mere-de-trois-detenus-decedee-vendredi-dans-un-accident_220110.html', '', 2),
(45, 'carrieres centrales', '2014-06-09 12:31:49', 13, 11, 'Un incendie nous rappelle que les lÃ©gendaires &lt;b&gt;CarriÃ¨res Centrales&lt;/b&gt; &lt;b&gt;...&lt;/b&gt;', '21 sept. 2009 &lt;b&gt;...&lt;/b&gt; Un incendie a dÃ©truit vendredi une soixantaine de baraques dans le bidonville &quot;\r\nAl Qabla &quot;, situÃ© Ã  Hay Mohammadi , mais sans faire deÂ ...', 'http://www.libe.ma/Un-incendie-nous-rappelle-que-les-legendaires-Carrieres-Centrales-sont-toujours-en-baraques-et-taudis_a5543.html', '', 0),
(46, 'carrieres centrales', '2014-06-09 12:39:07', 13, 0, 'Trois blessÃ©s dans une manifestation Ã  Hay Mohammadi - LibÃ©ration', '25 nov. 2010 &lt;b&gt;...&lt;/b&gt; Les anciens habitants des &lt;b&gt;CarriÃ¨res Centrales&lt;/b&gt; ne comptent pas baisser les bras. \r\nLeurs protestations se multiplient. Un sit-in accompagnÃ©Â ...', 'http://www.libe.ma/Trois-blesses-dans-une-manifestation-a-Hay-Mohammadi-Les-anciens-habitants-des-Carrieres-Centrales-montent-au-creneau_a15352.html', '', 0),
(47, 'carrieres centrales', '2014-06-09 12:45:57', 13, 11, '6 blessÃ©s et des dÃ©gÃ¢ts estimÃ©s Ã  4.000.000 de DH : Le marchÃ© des &lt;b&gt;...&lt;/b&gt;', '11 dÃ©c. 2009 &lt;b&gt;...&lt;/b&gt; Ce matin, le marchÃ© des &lt;b&gt;CarriÃ¨res centrales&lt;/b&gt;, Ã  Hay Mohammadi a l''air d''un \r\nchamp de ruines. Des dÃ©bris calcinÃ©s et des habitants qui ont duÂ ...', 'http://www.libe.ma/6-blesses-et-des-degats-estimes-a-4-000-000-de-DH-Le-marche-des-Carrieres-centrales-a-feu-et-a-sang_a7299.html', '', 0),
(48, 'carrieres centrales', '2014-06-10 18:17:33', 8, 0, 'Al Omrane Casablanca tient son assemblÃ©e gÃ©nÃ©rale : 6.862 unitÃ©s &lt;b&gt;...&lt;/b&gt;', '28 mai 2014 &lt;b&gt;...&lt;/b&gt; De mÃªme, le transfert des mÃ©nages des &lt;b&gt;CarriÃ¨res centrales&lt;/b&gt; a Ã©tÃ© effectuÃ© Ã  ... De \r\nson cÃ´tÃ©, Â«karianeÂ» (ou carriÃ¨res) Thomas comptait plus deÂ ...', 'http://www.lematin.ma/journal/2014/al-omrane-casablanca-tient-son-assemblee-generale_6862-unites-de-logement-seront-achevees-en-2014/203130.html', '', 0),
(49, 'carrieres centrales', '2014-06-13 09:57:15', 5, 0, 'ActualitÃ© | Aujourd''hui le Maroc', '19 dÃ©c. 2005 &lt;b&gt;...&lt;/b&gt; S''agissant de l''Ã©tude architecturale et paysagÃ¨re du parc &lt;b&gt;central&lt;/b&gt; de la .... journÃ©e \r\nd''Ã©tude sur le rÃ´le des &lt;b&gt;carriÃ¨res&lt;/b&gt; dans le dÃ©veloppement de laÂ ...', 'http://www.aujourdhui.ma/une/immobilier/actualite-86221', '', 0),
(50, 'carrieres centrales', '2014-06-13 09:57:19', 5, 0, 'Un incendie fait 20 blessÃ©s aux Â«&lt;b&gt;CarriÃ¨res centrales&lt;/b&gt;Â»', '22 juin 2010 &lt;b&gt;...&lt;/b&gt; L''incendie qui a ravagÃ© des &lt;b&gt;CarriÃ¨res centrales&lt;/b&gt; remet sur la table le problÃ¨me \r\nque posent les bidonvilles Ã  Casablanca. La capitaleÂ ...', 'http://www.aujourdhui.ma/une/focus/un-incendie-fait-20-blesses-aux-%25C2%25ABcarrieres-centrales%25C2%25BB-71421', '', 0),
(52, 'Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©', '2014-06-17 16:00:18', 3, 0, 'Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ø© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ &lt;b&gt;...&lt;/b&gt; - Ø§Ù„Ø±Ø¦ÙŠØ³ÙŠØ©', '12 Ø£ÙŠÙ„ÙˆÙ„ (Ø³Ø¨ØªÙ…Ø¨Ø±) 2013 &lt;b&gt;...&lt;/b&gt; Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ù‡ &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ Ø°Ø®Ø§Ø¦Ø±.. ... Ø¨Ø§Ù„Ø·Ø±ÙŠÙ‚ \r\nØ§Ù„Ø«Ø§Ù†ÙˆÙŠ Ø§Ù„Ù‚Ø±ÙŠØ¨Ø© Ù…Ù† Ø±ÙŠØ¬ÙŠ Ø·Ø§Ø¨Ø§ Ø¨Ø¨Ù„Ø¯ÙŠØ© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø§Ù„ØªØ§Ø¨Ø¹Ø© Ù„Ø¹Ù…Ø§Ù„Ø© Ø§Ù„Ù…Ø­Ù…Ø¯ÙŠØ©.', 'http://static.hibapress.com/details-9322.html', '', 0),
(53, 'Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©', '2014-06-17 16:00:18', 3, 0, 'Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ø© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ &lt;b&gt;...&lt;/b&gt; - Ø§Ù„Ø±Ø¦ÙŠØ³ÙŠØ©', '12 Ø£ÙŠÙ„ÙˆÙ„ (Ø³Ø¨ØªÙ…Ø¨Ø±) 2013 &lt;b&gt;...&lt;/b&gt; Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ù‡ &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ Ø°Ø®Ø§Ø¦Ø±.. ... Ø¨Ø§Ù„Ø·Ø±ÙŠÙ‚ \r\nØ§Ù„Ø«Ø§Ù†ÙˆÙŠ Ø§Ù„Ù‚Ø±ÙŠØ¨Ø© Ù…Ù† Ø±ÙŠØ¬ÙŠ Ø·Ø§Ø¨Ø§ Ø¨Ø¨Ù„Ø¯ÙŠØ© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø§Ù„ØªØ§Ø¨Ø¹Ø© Ù„Ø¹Ù…Ø§Ù„Ø© Ø§Ù„Ù…Ø­Ù…Ø¯ÙŠØ©.', 'http://static.hibapress.com/details-9322.html', '', 0),
(54, 'Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©', '2014-06-17 16:00:19', 3, 0, 'Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ø© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ &lt;b&gt;...&lt;/b&gt; - Ø§Ù„Ø±Ø¦ÙŠØ³ÙŠØ©', '12 Ø£ÙŠÙ„ÙˆÙ„ (Ø³Ø¨ØªÙ…Ø¨Ø±) 2013 &lt;b&gt;...&lt;/b&gt; Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ù‡ &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ Ø°Ø®Ø§Ø¦Ø±.. ... Ø¨Ø§Ù„Ø·Ø±ÙŠÙ‚ \r\nØ§Ù„Ø«Ø§Ù†ÙˆÙŠ Ø§Ù„Ù‚Ø±ÙŠØ¨Ø© Ù…Ù† Ø±ÙŠØ¬ÙŠ Ø·Ø§Ø¨Ø§ Ø¨Ø¨Ù„Ø¯ÙŠØ© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø§Ù„ØªØ§Ø¨Ø¹Ø© Ù„Ø¹Ù…Ø§Ù„Ø© Ø§Ù„Ù…Ø­Ù…Ø¯ÙŠØ©.', 'http://static.hibapress.com/details-9322.html', '', 0),
(55, 'Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©', '2014-06-17 16:00:20', 3, 0, 'Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ø© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ &lt;b&gt;...&lt;/b&gt; - Ø§Ù„Ø±Ø¦ÙŠØ³ÙŠØ©', '12 Ø£ÙŠÙ„ÙˆÙ„ (Ø³Ø¨ØªÙ…Ø¨Ø±) 2013 &lt;b&gt;...&lt;/b&gt; Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ù‡ &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ Ø°Ø®Ø§Ø¦Ø±.. ... Ø¨Ø§Ù„Ø·Ø±ÙŠÙ‚ \r\nØ§Ù„Ø«Ø§Ù†ÙˆÙŠ Ø§Ù„Ù‚Ø±ÙŠØ¨Ø© Ù…Ù† Ø±ÙŠØ¬ÙŠ Ø·Ø§Ø¨Ø§ Ø¨Ø¨Ù„Ø¯ÙŠØ© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø§Ù„ØªØ§Ø¨Ø¹Ø© Ù„Ø¹Ù…Ø§Ù„Ø© Ø§Ù„Ù…Ø­Ù…Ø¯ÙŠØ©.', 'http://static.hibapress.com/details-9322.html', '', 0),
(56, 'Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©', '2014-06-17 16:00:21', 3, 0, 'Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ø© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ &lt;b&gt;...&lt;/b&gt; - Ø§Ù„Ø±Ø¦ÙŠØ³ÙŠØ©', '12 Ø£ÙŠÙ„ÙˆÙ„ (Ø³Ø¨ØªÙ…Ø¨Ø±) 2013 &lt;b&gt;...&lt;/b&gt; Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ù‡ &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ Ø°Ø®Ø§Ø¦Ø±.. ... Ø¨Ø§Ù„Ø·Ø±ÙŠÙ‚ \r\nØ§Ù„Ø«Ø§Ù†ÙˆÙŠ Ø§Ù„Ù‚Ø±ÙŠØ¨Ø© Ù…Ù† Ø±ÙŠØ¬ÙŠ Ø·Ø§Ø¨Ø§ Ø¨Ø¨Ù„Ø¯ÙŠØ© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø§Ù„ØªØ§Ø¨Ø¹Ø© Ù„Ø¹Ù…Ø§Ù„Ø© Ø§Ù„Ù…Ø­Ù…Ø¯ÙŠØ©.', 'http://static.hibapress.com/details-9322.html', '', 0),
(58, 'Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©', '2014-06-17 16:00:32', 3, 1, 'Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ø© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ &lt;b&gt;...&lt;/b&gt; - Ù‡Ø¨Ø© Ø¨Ø±ÙŠØ³', '12 Ø£ÙŠÙ„ÙˆÙ„ (Ø³Ø¨ØªÙ…Ø¨Ø±) 2013 &lt;b&gt;...&lt;/b&gt; Ø§Ø³ØªÙ†ÙØ§Ø± ÙˆÙ‡Ù„Ø¹ Ø³Ø§ÙƒÙ†Ø© &lt;b&gt;Ø¹ÙŠÙ† Ø­Ø±ÙˆØ¯Ø©&lt;/b&gt; Ø¨Ø¹Ø¯ Ø¹Ø«ÙˆØ± Ø§Ù„Ø¯Ø±Ùƒ Ø¹Ù„Ù‰ Ø£Ø³Ù„Ø­Ø© Ùˆ Ø°Ø®Ø§Ø¦Ø±.. Ø³ÙÙŠØ§Ù† Ø§Ù„Ø¹Ù„Ù…ÙŠ Ù€ Ù‡Ø¨Ø© \r\nØ¨Ø±ÙŠØ³. ØªÙ… Ø§Ù„Ø¹Ø«ÙˆØ± ÙŠÙˆÙ… Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡ 11 Ø³Ø¨ØªÙ…Ø¨Ø± 2013 Ø¹Ù„Ù‰ Ø§Ù„Ø³Ø§Ø¹Ø©Â ...', 'http://www.hibapress.com/adm/m/details-9322.html', '', 1),
(59, 'Ø³ÙŠØ¯ÙŠ Ø§Ù„Ø¨Ø±Ù†ÙˆØµÙŠ ', '2014-06-24 16:23:23', 2, 12, '&quot;Ù„ÙŠØ¯ÙŠÙƒ&quot;: Ø§Ù„Ø¯Ø§Ø± Ø§Ù„Ø¨ÙŠØ¶Ø§Ø¡ ØªØªØ®Ù„Øµ Ù…Ù† ØªÙ„ÙˆØ« Ø´ÙˆØ§Ø·Ø¦Ù‡Ø§ ÙÙŠ Ø¯Ø¬Ù†Ø¨Ø± Ø§Ù„Ù…Ù‚Ø¨Ù„', '16 Ø£ÙŠØ§Ø± (Ù…Ø§ÙŠÙˆ) 2014 &lt;b&gt;...&lt;/b&gt; Ù…Ø­Ø·Ø© Ø§Ù„Ù…Ø¹Ø§Ù„Ø¬Ø© Ø§Ù„Ù‚Ø¨Ù„ÙŠØ© &lt;b&gt;Ø¨Ø³ÙŠØ¯ÙŠ Ø§Ù„Ø¨Ø±Ù†ÙˆØµÙŠ&lt;/b&gt;. ÙˆØ¹Ù† Ù‚Ø¯Ø±Ø© Ø§Ù„Ù…Ø­Ø·Ø© Ù„Ù„Ù…Ø¹Ø§Ù„Ø¬Ø© Ø§Ù„Ù‚Ø¨Ù„ÙŠØ©ØŒ Ù‚Ø§Ù„ \r\nØ§Ù„Ù…Ø³Ø¤ÙˆÙ„ÙˆÙ† Ø¥Ù†Ù‡ ØªÙ… ØªØµÙ…ÙŠÙ… Ø§Ù„Ù‡Ù†Ø¯Ø³Ø© Ø§Ù„Ù…Ø¯Ù†ÙŠØ© Ù…Ù† Ø£Ø¬Ù„ ØµØ¨ÙŠØ¨ Ø°Ø±ÙˆØªÙ‡ 11 Ù…ØªØ± Ù…ÙƒØ¹Ø¨Â ...', 'http://www.hespress.com/regions/208821.html', '', 1),
(60, 'pharmaciens', '2014-07-01 10:39:03', 6, 0, 'GrÃ¨ve des &lt;b&gt;pharmaciens&lt;/b&gt; casablancais: Le Wali &lt;b&gt;...&lt;/b&gt; - Aufait Maroc', 'il y a 22 heures &lt;b&gt;...&lt;/b&gt; Mercredi dernier, les &lt;b&gt;pharmaciens&lt;/b&gt; de la wilaya du Grand Casablanca \r\ndÃ©nonÃ§aient, dans un communiquÃ©, une â€œanarchie crÃ©Ã©e par leÂ ...', 'http://www.aufaitmaroc.com/maroc/societe/2014/6/30/le-wali-rencontre-les-representants-du-syndicat_220432.html', '', 0),
(61, 'pharmaciens', '2014-07-01 10:40:03', 6, 0, 'GrÃ¨ve des &lt;b&gt;pharmaciens&lt;/b&gt; casablancais: Le Wali &lt;b&gt;...&lt;/b&gt; - Aufait Maroc', 'il y a 23 heures &lt;b&gt;...&lt;/b&gt; Mercredi dernier, les &lt;b&gt;pharmaciens&lt;/b&gt; de la wilaya du Grand Casablanca \r\ndÃ©nonÃ§aient, dans un communiquÃ©, une â€œanarchie crÃ©Ã©e par leÂ ...', 'http://m.aufaitmaroc.com/maroc/societe/2014/6/30/le-wali-rencontre-les-representants-du-syndicat_220432.html', '', 0),
(62, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 09:49:49', 2, 0, 'Ø§Ù„Ù†Ø³Ø§Ø¡ Ø£ÙƒØ«Ø± Ø¶Ø­Ø§ÙŠØ§ ÙØ§Ø¬Ø¹Ø© &quot;&lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt;&quot; .. ÙˆÙØ±Øµ Ù†Ø¬Ø§Ø© Ø¢Ø®Ø±ÙŠÙ† Ø¶Ø¦ÙŠÙ„Ø© - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° ÙŠÙˆÙ… 1 &lt;b&gt;...&lt;/b&gt; ÙÙŠÙ…Ø§ ØªØªÙˆØ§ØµÙ„ Ø£Ø¹Ù…Ø§Ù„ Ø§Ù„Ø¨Ø­Ø« Ø¹Ù† Ù†Ø§Ø¬ÙŠÙ† Ù…Ø­ØªÙ…Ù„ÙŠÙ† Ø£Ùˆ Ø¬Ø«Ø« Ø¶Ø­Ø§ÙŠØ§ Ø§Ù†Ù‡ÙŠØ§Ø± Ø¹Ù…Ø§Ø±Ø§Øª &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt;ØŒ Ø£ÙƒØ¯ \r\nØ§Ù„Ø¯ÙƒØªÙˆØ± Ù…ØµØ·ÙÙ‰ Ø§Ù„Ø±Ø¯Ø§Ø¯ÙŠØŒ Ø§Ù„Ù…Ø¯ÙŠØ± Ø§Ù„Ø¬Ù‡ÙˆÙŠ Ù„Ù„ØµØ­Ø© Ø¨Ø¬Ù‡Ø© Ø§Ù„Ø¯Ø§Ø± Ø§Ù„Ø¨ÙŠØ¶Ø§Ø¡ØŒÂ ...', 'http://www.hespress.com/faits-divers/235669.html', '', 0),
(63, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 09:49:52', 2, 0, 'ÙÙŠ Ø£Ù†Ù‚Ø§Ø¶ Ø¹Ù…Ø§Ø±Ø§Øª &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° 11 Ø³Ø§Ø¹Ø© &lt;b&gt;...&lt;/b&gt; ÙÙŠ Ø£Ù†Ù‚Ø§Ø¶ Ø¹Ù…Ø§Ø±Ø§Øª &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt;. ... Ø§Ù„Ù…Ù„Ùƒ Ù…Ø­Ù…Ø¯ Ø§Ù„Ø³Ø§Ø¯Ø³ ÙŠØ²ÙˆØ± Ø§Ù„Ù…ÙØµØ§Ø¨ÙŠÙ† ÙÙŠ Ø§Ù†Ù‡ÙŠØ§Ø± &quot;Ø¹Ù…ÙŽØ§Ø±ÙŽØ§Øª \r\n&lt;b&gt;Ø¨ÙÙˆØ±ÙƒÙÙˆÙ†&lt;/b&gt;&quot; Â· ØªÙ‚Ø±ÙŠØ± ÙŠÙ†ØªÙ‚Ø¯ ØªØ¹Ø§Ù…Ù„ Ù…Ø¯Ø±ÙŠØ¯ ÙˆØ§Ù„Ø±Ø¨Ø§Ø· Ù…Ø¹ Ù…Ù‡Ø§Ø¬Ø±ÙŠ Ø¬Ù†ÙˆØ¨ Ø§Ù„ØµØ­Ø±Ø§Ø¡.', 'http://www.hespress.com/videos/235747.html', '', 0),
(64, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 09:49:57', 2, 0, 'Ø¥ÙŠÙˆØ§Ø¡ Ø¶Ø­Ø§ÙŠØ§ Ø§Ù†Ù‡ÙŠØ§Ø± &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° ÙŠÙˆÙ… 1 &lt;b&gt;...&lt;/b&gt; Qui dit mieux. Y a t il un prÃ©sident ou un roi qui est tjrs proche de son peuple \r\ncomme le fait sa MajestÃ© Med VI ? C''est un roi digne de ce nomÂ ...', 'http://www.hespress.com/videos/235685.html', '', 0),
(65, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 09:50:01', 2, 0, 'Ø§Ù„Ù…Ù„Ùƒ ÙŠØªÙÙ‚Ø¯ Ø¬Ø±Ø­Ù‰ &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° ÙŠÙˆÙ… 1 &lt;b&gt;...&lt;/b&gt; vraiment un trÃ¨s grand cadeau offert Ã  tous les marocains ce cadeau est sans \r\nhÃ©sitation ce aimable roi Ã©lÃ©gant, sage, cultivÃ©, jeune, moderne,Â ...', 'http://www.hespress.com/videos/235701.html', '', 0),
(66, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 09:50:04', 2, 0, 'Ø§Ù„Ù…Ù„Ùƒ ÙˆØ³Ø· Ø§Ù†Ù‡ÙŠØ§Ø± &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° 2 ÙŠÙˆÙ… &lt;b&gt;...&lt;/b&gt; Ø§Ù„Ù…Ù„Ùƒ ÙˆØ³Ø· Ø§Ù†Ù‡ÙŠØ§Ø± &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt;. ... Ø§Ù„Ù†Ø³Ø§Ø¡ Ø£ÙƒØ«Ø± Ø¶Ø­Ø§ÙŠØ§ ÙØ§Ø¬Ø¹Ø© &quot;&lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt;&quot; .. ÙˆÙØ±Øµ Ù†Ø¬Ø§Ø© Ø¢Ø®Ø±ÙŠÙ† \r\nØ¶Ø¦ÙŠÙ„Ø© Â· Ø­ØµÙŠÙ„Ø© Ø§Ù†Ù‡ÙŠØ§Ø± &quot;Ø¹Ù…ÙŽØ§Ø±ÙŽØ§Øª &lt;b&gt;Ø¨ÙÙˆØ±ÙƒÙÙˆÙ†&lt;/b&gt;&quot; ØªØ±ØªÙØ¹ Ø¥Ù„Ù‰ 8 Ù‚ØªÙ„Ù‰ Â· Ø§Ù„Ù…Ù„ÙƒÂ ...', 'http://www.hespress.com/videos/235608.html', '', 0),
(67, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 09:50:08', 2, 0, 'Ø§Ù†Ù‡ÙŠØ§Ø± &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; Ø¹Ù„Ù‰ ÙØ±Ø§Ù†Ø³24 - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° 2 ÙŠÙˆÙ… &lt;b&gt;...&lt;/b&gt; Ù‡Ø°Ù‡ Ù†ØªØ§Ø¦Ø¬ Ø§Ù„Ø¨Ù†Ø§Ø¡ Ø§Ù„Ø¹Ø´ÙˆØ§Ø¦ÙŠ ÙˆØªØ³Ø§Ù‡Ù„ Ø§Ù„Ø³Ù„Ø·Ø§Øª Ø§Ù„Ù…Ø­Ù„ÙŠØ© Ù…Ø¹Ø§Ù‡ Ù…Ù‚Ø§Ø¨Ù„ Ø§Ù„Ø±Ø´ÙˆØ© ÙˆØ§Ù„ÙØ³Ø§Ø¯ Ù…Ù† Ø¨Ø¹Ø¶ \r\nÙ…Ù†Ø¹Ø¯Ù…ÙŠ Ø§Ù„Ø¶Ù…ÙŠØ± Ù„Ø§ ØªÙ‡Ù…Ù‡Ù… Ø£Ø±ÙˆØ§Ø­ Ø¹Ø¨Ø§Ø¯ Ø§Ù„Ù„Ù‡ . ÙÙ„Ùˆ Ø­Ø¯ØªØª Ù‡Ø² Ø£Ø±Ø¶ÙŠØ© Ø¨Ù‚ÙˆØ©Â ...', 'http://www.hespress.com/videos/235631.html', '', 0),
(68, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 09:59:00', 2, 0, 'ÙØ§Ø¬Ø¹Ø© Ø§Ù†Ù‡ÙŠØ§Ø± Ø­ÙŠÙ‘ &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° 2 ÙŠÙˆÙ… &lt;b&gt;...&lt;/b&gt; ÙØ§Ø¬Ø¹Ø© Ø§Ù†Ù‡ÙŠØ§Ø± Ø­ÙŠÙ‘ &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt;. ... Ø§ÙŠÙ† ÙƒØ§Ù†Øª Ø§Ù„Ù…Ø±Ø§Ù‚Ø¨Ø© Ù„Ù‡Ø°Ù‡ Ø§Ù„Ø¨ÙŠÙˆØª Ø®Ø§ØµØ© ÙˆØ§Ù„ÙƒÙ„ ÙŠØ¹Ù„Ù… Ø§Ù† ÙƒØ«ÙŠØ± Ù…Ù† \r\nÙ‡Ø§ØªÙ‡ Ø§Ù„Ø¨ÙŠÙˆØª (ÙÙŠ &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; Ø§Ùˆ Ø§Ù„Ù…Ø¯ÙŠÙ†Ø© Ø§Ù„Ù‚Ø¯ÙŠÙ…Ø©) Ø§ÙŠÙ„Ø© Ù„Ù„Ø³Ù‚ÙˆØ·.', 'http://www.hespress.com/videos/235594.html', '', 0),
(69, 'effondrements 3', '2014-07-14 10:00:32', 8, 0, 'L''&lt;b&gt;effondrement&lt;/b&gt; de trois immeubles Ã  Casablanca fait 4 morts et 55 &lt;b&gt;...&lt;/b&gt;', 'il y a 2Â jours &lt;b&gt;...&lt;/b&gt; Sa MajestÃ© le Roi Mohammed VI, accompagnÃ© de S.A.R. le Prince HÃ©ritier \r\nMoulay El Hassan, a visitÃ©, vendredi, les lieux de l''&lt;b&gt;effondrement&lt;/b&gt; deÂ ...', 'http://www.lematin.ma/journal/2014/effondrement-de-trois-immeubles-a-casablanca--sm-le-roi-visite-les-lieux-et-se-rend-au-chevet-des-victimes/205708.html', '', 0),
(70, 'effondrements 3', '2014-07-14 10:01:39', 8, 0, '&lt;b&gt;3&lt;/b&gt; morts et &lt;b&gt;3&lt;/b&gt; blessÃ©s dans l''&lt;b&gt;effondrement&lt;/b&gt; du toit d''une mosquÃ©e Ã  &lt;b&gt;...&lt;/b&gt;', '22 avr. 2011 &lt;b&gt;...&lt;/b&gt; Trois personnes ont trouvÃ© la mort et &lt;b&gt;3&lt;/b&gt; autres blessÃ©es, dont deux jugÃ©s en \r\nsituation grave, dans l''&lt;b&gt;effondrement&lt;/b&gt; partiel, jeudi matin, d''uneÂ ...', 'http://mobile.lematin.ma/content/1303475679986244900/MatinPA', '', 0),
(71, 'Ø¨ÙˆØ±ÙƒÙˆÙ†', '2014-07-14 10:06:54', 17, 0, 'Ø§Ù„Ø¬Ø±ÙŠØ¯Ø© 24', 'Ø§Ù†Ù‡ÙŠØ§Ø± Ø«Ù„Ø§Ø« Ø¹Ù…Ø§Ø±Ø§Øª Ø¨Ø­ÙŠ &lt;b&gt;Ø¨ÙˆØ±ÙƒÙˆÙ†&lt;/b&gt; Ø¨Ø§Ù„Ø¨ÙŠØ¶Ø§Ø¡ Â· Ø¥Ù†ÙØ±Ø§Ø¯ : Ù…Ø­Ù…Ø¯ ÙŠØ±ÙˆÙŠ ØªÙØ§ØµÙŠÙ„ Ø§Ù„Ø¥Ø¹ØªØ¯Ø§Ø¡ \r\nØ§Ù„Ø¬Ù†Ø³ÙŠ Ø¹Ù„Ù‰ Ø¥Ø¨Ù†Ù‡ Â· 10 : Ø¨Ø±Ø§Ù…Ø¬ Ø±Ù…Ø¶Ø§Ù† - Ù„ÙƒÙˆØ¨Ù„ Ø§Ù„Ø­Ù„Ù‚Ø© Â· Ø£Ù„Ù…Ø§Ù†ÙŠØ§ ØªØ³Ø­Ù‚ Ø§Ù„Ø¨Ø±Ø§Ø²ÙŠÙ„ ÙÙŠ \r\nÙ†ØµÙÂ ...', 'http://aljarida24.ma/', '', 0),
(72, 'bourgogne', '2014-07-17 09:56:16', 2, 15, 'Ù…Ø±ÙƒØ² Ø­Ù‚ÙˆÙ‚ÙŠ: Ù‡Ø°Ù‡ Ø£Ø³Ø¨Ø§Ø¨ Ø§Ù†Ù‡ÙŠØ§Ø± Ø¹Ù…Ø§Ø±Ø§Øª &quot;Ø¨ÙˆØ±ÙƒÙˆÙ†&quot; Ø¨Ø§Ù„Ø¯Ø§Ø± Ø§Ù„Ø¨ÙŠØ¶Ø§Ø¡ - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° 2 ÙŠÙˆÙ… &lt;b&gt;...&lt;/b&gt; 16 - weld &lt;b&gt;bourgogne&lt;/b&gt; Ø§Ù„Ø§Ø«Ù†ÙŠÙ† 14 ÙŠÙˆÙ„ÙŠÙˆØ² 2014 - 13:40 ... On a des video prouvant \r\nque sans l aide des jeunes de &lt;b&gt;bourgogne&lt;/b&gt; rien ne pouvait etreÂ ...', 'http://www.hespress.com/societe/235799.html', '', 0),
(73, 'bourgogne', '2014-07-17 09:58:19', 2, 15, 'Ø§Ù„Ù…Ù„Ùƒ ÙŠØ²ÙˆØ± Ù…ÙˆÙ‚Ø¹ Ø§Ù†Ù‡ÙŠØ§Ø± &quot;Ø¹Ù…ÙŽØ§Ø±ÙŽØ§Øª Ø¨ÙÙˆØ±ÙƒÙÙˆÙ†&quot; ÙˆÙŠØªÙƒÙÙ„ Ø¨Ø¹Ù„Ø§Ø¬ Ø§Ù„Ù…ØµØ§Ø¨ÙŠÙ†', 'Ù…Ù†Ø° 5 ÙŠÙˆÙ… &lt;b&gt;...&lt;/b&gt; ... il faut d abord bien s informÃ©. .le cas Ã©chÃ©ant, les sanctions des responsables \r\nde ce drame humain a &lt;b&gt;bourgogne&lt;/b&gt; y sont dÃ©jÃ  personne n estÂ ...', 'http://www.hespress.com/faits-divers/235590.html', '', 0),
(74, 'CASABLANCA', '2014-07-17 10:04:09', 6, 15, 'Drame de Bourgogne Ã  &lt;b&gt;Casablanca&lt;/b&gt;: L''autre face des opÃ©rations de &lt;b&gt;...&lt;/b&gt;', 'il y a 2Â jours &lt;b&gt;...&lt;/b&gt; Les opÃ©rations de sauvetage se sont poursuivies, lundi matin, sur le lieu de l''\r\neffondrement des trois immeubles au quartier Bourgogne Ã Â ...', 'http://www.aufaitmaroc.com/maroc/societe/2014/7/14/lautre-face-des-operations-de-secours-_220648.html', '', 0),
(75, 'Ù…ØµØ±', '2014-07-21 12:12:36', 2, 1, 'Ø³ÙÙŠØ± &lt;b&gt;Ù…ØµØ±&lt;/b&gt; Ø¨Ø§Ù„Ø±Ø¨Ø§Ø· ÙŠØ±ÙØ¶ Ø§Ù„Ø¥Ø³Ø§Ø¡Ø© Ù„Ù„Ù…ØºØ±Ø¨ ÙˆÙŠØ¹Ø¯ Ø¨Ø¹Ø¯Ù… ØªÙƒØ±Ø§Ø±Ù‡Ø§ - Ù‡Ø³Ø¨Ø±ÙŠØ³', 'Ù…Ù†Ø° 3 ÙŠÙˆÙ… &lt;b&gt;...&lt;/b&gt; Ù…Ø«Ù„ ÙƒØ±Ø© Ø«Ù„Ø¬ ØªØ¨Ø¯Ø£ ØµØºÙŠØ±Ø© Ù„ØªÙƒØ¨Ø± Ø¨ØªØ¯Ø­Ø±Ø¬Ù‡Ø§ØŒ Ø£ÙØ¶Øª ØªØµØ±ÙŠØ­Ø§Øª Ø§Ù„Ù…Ø°ÙŠØ¹Ø© &lt;b&gt;Ø§Ù„Ù…ØµØ±ÙŠØ©&lt;/b&gt;ØŒ Ø£Ù…Ø§Ù†ÙŠ \r\nØ§Ù„Ø®ÙŠØ§Ø·ØŒ ÙÙŠ Ù‚Ù†Ø§Ø© &quot;Ø£ÙˆÙ† ØªÙŠ ÙÙŠ&quot;ØŒ Ø¨Ø§Ù„Ø­Ø¯ÙŠØ« Ø¹Ù† Ø¯ÙˆØ± &quot;Ù…Ù„ØªØ¨Ø³&quot; Ù„Ù„Ø¹Ø§Ù‡Ù„Â ...', 'http://www.hespress.com/orbites/236113.html', '', 1),
(76, 'taxis', '2014-07-25 10:21:46', 2, 16, 'Ø¨Ù†Ø´Ù…Ø§Ø³: Ù‚Ø±ÙŠØ¨Ø§ Ø³ÙŠØªÙ…Ù‘ ØªØ¬Ø¯ÙŠØ¯ Ø£Ø³Ø·ÙˆÙ„ Ø³ÙŠØ§Ø±Ø§Øª Ø§Ù„Ø£Ø¬Ø±Ø© Ø§Ù„ÙƒØ¨ÙŠØ±Ø© Ø¹Ù† Ø¢Ø®Ø±Ù‡', '25 Ø¢Ø°Ø§Ø± (Ù…Ø§Ø±Ø³) 2014 &lt;b&gt;...&lt;/b&gt; avec DACIA et RENAUT comme grand &lt;b&gt;taxi&lt;/b&gt; au maroc il faut s''attendre au double \r\n... J''espÃ¨re que les nouveaux &lt;b&gt;taxis&lt;/b&gt; ont la ceinture de sÃ©curitÃ©.', 'http://www.hespress.com/societe/165421.html', '', 0),
(77, 'taxis', '2014-07-25 10:22:32', 2, 0, 'Ø¥Ø´ÙƒØ§Ù„ÙŠØ© Ø§Ù„Ù†Ù‚Ù„ Ø¨Ø§Ù„Ø¨ÙŠØ¶Ø§Ø¡ - Ù‡Ø³Ø¨Ø±ÙŠØ³', '3 ØªÙ…ÙˆØ² (ÙŠÙˆÙ„ÙŠÙˆ) 2014 &lt;b&gt;...&lt;/b&gt; Replace all small polluting &lt;b&gt;taxis&lt;/b&gt; with eco ones 3. Lenghten the tramway lines to \r\nnew districts 4. Ban all heavy-goods vehicles from entering theÂ ...', 'http://www.hespress.com/videos/234935.html', '', 0),
(78, 'taxis', '2014-07-25 10:31:03', 8, 16, 'RenouvÃ¨lement du parc des grands &lt;b&gt;taxis&lt;/b&gt; : Les professionnels &lt;b&gt;...&lt;/b&gt;', 'il y a 5Â jours &lt;b&gt;...&lt;/b&gt; AdoptÃ© en Conseil de gouvernement, le projet de dÃ©cret sur le renouvÃ¨lement du \r\nparc des grands &lt;b&gt;taxis&lt;/b&gt; ne fait pas l''unanimitÃ© au sein desÂ ...', 'http://www.lematin.ma/journal/-/206208.html', '', 0),
(79, 'taxis', '2014-07-25 10:31:10', 8, 0, 'JournÃ©e de sensibilisation avec les reprÃ©sentants des &lt;b&gt;taxis&lt;/b&gt; - Le Matin', '15 juin 2014 &lt;b&gt;...&lt;/b&gt; Tous les intervenants des diffÃ©rents corps de la police ont louÃ© les efforts \r\ndÃ©ployÃ©s par les reprÃ©sentants des conducteurs de &lt;b&gt;taxis&lt;/b&gt; pour laÂ ...', 'http://www.lematin.ma/journal/-/204185.html', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `synthese` text NOT NULL,
  `titreevent` int(11) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `titreevent` (`titreevent`),
  KEY `titreevent_2` (`titreevent`),
  KEY `titreevent_3` (`titreevent`),
  KEY `titreevent_4` (`titreevent`),
  KEY `titreevent_5` (`titreevent`),
  KEY `titreevent_6` (`titreevent`),
  KEY `titreevent_7` (`titreevent`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `titre`, `synthese`, `titreevent`, `url`) VALUES
(1, 'rter', 'ttre', 12, 'tretrtrt'),
(2, 'rfgref', 'dfdsfd', 13, 'upload/ALS-ice-bucket-challenge-obama-.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `login`
--

INSERT INTO `login` (`id`, `nom`, `prenom`, `email`, `name`, `pass`) VALUES
(1, 'Anass', 'Mohammed', 'a.anas@jaweb.ma', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Structure de la table `prefecteur`
--

CREATE TABLE IF NOT EXISTS `prefecteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `prefecteur`
--

INSERT INTO `prefecteur` (`id`, `name`) VALUES
(1, 'Sidi Bernoussi'),
(2, 'Hay Mohamedi'),
(3, 'Mers Sultan'),
(4, 'ANFA'),
(5, 'hay al hassani');

-- --------------------------------------------------------

--
-- Structure de la table `secteur`
--

CREATE TABLE IF NOT EXISTS `secteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Contenu de la table `secteur`
--

INSERT INTO `secteur` (`id`, `name`) VALUES
(1, 'Culture'),
(2, 'Arboriculture'),
(3, 'Elevage'),
(4, 'Culture et élevage associés'),
(5, 'Services annexes à l''agriculture'),
(6, 'Chasse'),
(7, 'Sylviculture, exploitation forestière, services annexes'),
(8, 'Peche, aquaculture'),
(9, 'Extraction de houille, de lignite et de tourbe'),
(10, 'Extraction d''hydrocarbures'),
(11, 'URBANISME'),
(12, 'L''Environnement '),
(13, 'l''environnement '),
(14, 'santÃ©'),
(15, 'habitat menacant ruine et bidonville'),
(16, 'taxis'),
(17, 'transport');

-- --------------------------------------------------------

--
-- Structure de la table `siteweb`
--

CREATE TABLE IF NOT EXISTS `siteweb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `siteweb`
--

INSERT INTO `siteweb` (`id`, `name`, `link`) VALUES
(2, 'hespress', 'hespress.com'),
(3, 'Hibapress', 'hibapress.com'),
(5, 'Aujourdhui', 'aujourdhui.ma'),
(6, 'Aufaitmaroc', 'aufaitmaroc.com'),
(7, 'lnt', 'lnt.ma'),
(8, 'Lematin', 'lematin.ma'),
(9, 'sabah', 'www.assabah.ma'),
(12, 'ÙƒÙˆØ¯', 'www.goud.ma'),
(13, 'liberation', 'www.libe.ma'),
(14, 'casaalaan', 'http://www.casaalaan.com/'),
(15, 'ÙƒÙŠÙØ§Ø´', 'http://www.kifache.com/'),
(16, 'ÙƒØ§Ø²Ø§ÙˆÙŠ', 'http://casaoui.ma/'),
(17, 'Ø§Ù„Ø¬Ø±ÙŠØ¯Ø© 24', 'http://aljarida24.ma'),
(18, 'Maghreb arabe presse', 'http://www.map.ma'),
(19, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `titreevent`
--

CREATE TABLE IF NOT EXISTS `titreevent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `titreevent`
--

INSERT INTO `titreevent` (`id`, `titre`, `date`) VALUES
(12, 'borgounne', '2014-09-10'),
(13, 'casa', '2014-09-03');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`titreevent`) REFERENCES `titreevent` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
