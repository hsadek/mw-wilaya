<?php ob_start();session_start();
if (isset($_SESSION["username"])) {
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">

    <title>monitoring mobile </title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <script src="jquery-1.9.0.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<section id="container" >
<?php include_once("header.php"); 
//pour couréger le pb des accents francaises
//il faudrais d'ajouter toujour cet mourceau de code en haut de page pour que il puisse fonctionner correctement 
mysql_query("SET NAMES 'utf8'");
?>
<aside>
    <div id="sidebar" class="nav-collapse">

      <?php 
      $ul="c";

      include_once("sidebar.php");
     ?>
    </div>
</aside>
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Liste des classes
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
<?php  if (isset($_SESSION["modif"])) { ?>
<div class="alert alert-success fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Modification! </strong>votre modification a été enregistrée..
</div>
<?php } unset($_SESSION["modif"]);?>
<?php  if (isset($_SESSION["del"])) { ?>
<div class="alert alert-success fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                               <strong>Supprission! </strong>votre modification a été supprimée..
</div>
<?php } unset($_SESSION["del"]);?>
                    <div class="panel-body">
    <div class="pull-left" style="margin: 0 20px 20px 0;">
<a href="addcategorie.php" class="btn btn-success">ajouter <i class="fa fa-plus"></i></a>
</div>
                    
                                         <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
                                <thead>
                                <tr role="row">
                                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="First Name" style="width: 170px;">#</th>
                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending" style="width: 166px;">Libelle</th>
                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending" style="width: 166px;">Détails</th>
   
  <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending" style="width: 166px;">Action</th>


                            </tr>
                                </thead>

                                
                            <tbody role="alert" aria-live="polite" aria-relevant="all">

<!--Confirmation de suppresion -->

<script type="text/javascript">
    $(function() {

        $('a#del').click(

            function() {

                 var msg=confirm("êtes vous sûre de supprimer l'enregistrement?");

                 if (msg) {

                        <?php 

                            if (isset($_GET["del"])) {
                            $id=$_GET["id"];
                            $requete="DELETE FROM categories WHERE id=".$id."";
                            $resultat=mysql_query($requete);
                            $_SESSION["del"]=1;
                            header("Location:categorie.php");
                            } 

                        ?>
                    }
                    else {

                        return false;
                    }

                   
            }
            );

    });

</script>                        
<?php 

$req2="SELECT count(id) as nbrCat FROM categories";
$t=mysql_query($req2);
$data=mysql_fetch_assoc($t);
$nbrCat=$data['nbrCat'];
$nbrPerPage=10;
$nbrp=ceil($nbrCat/$nbrPerPage);
$nbPageLimit = 4;
$pOffset = 2; // Nombre de pages à afficher avant les switches.
$lastPage = ((int)(($nbrCat + $nbrPerPage - 1)/$nbrPerPage )); // calcul de la dernière page.
if (isset($_GET['p']) && $_GET['p']>0 && $_GET['p']<=$nbrp) {

    $cPage=$_GET['p'];
}else{
    $cPage=1;
}

$requete="SELECT * FROM categories limit ".(($cPage-1)*$nbrPerPage).",".$nbrPerPage."";
$resultat=mysql_query($requete);

while ($r=mysql_fetch_assoc($resultat)) { ?>

   </tr>
        <tr class="odd">
        <td class="#"><?php echo $r["id"]; ?></td>
        <td class="#"><?php echo  $r["categorie_libelle"]; ?></td>
        <td class="#"><?php echo substr($r["detail"],0,100); ?>...</td>
        <td class="#">
<a href="editcategorie.php?id=<?php echo $r['id']; ?>" class="btn btn-warning"><i class="fa  fa-edit"></i></a>
<a href="categorie.php?del=1&id=<?php echo $r["id"]; ?>" class="btn btn-danger" id="del"><i class="fa fa-times-circle"></i></a>
<a href="details.php?id=<?php echo $r["id"]; ?>" class="btn btn-info">détails</a>
        </td>
      
                                    
    </tr>
   
<?php } unset($_SESSION["modif"]);?> 

                                </tbody>
                                </table>
                                <!--<div class="row"><div class="col-lg-6"><div class="dataTables_info" id="editable-sample_info">Showing 1 to 5 of 28 entries</div></div><div class="col-lg-6"><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#">← Prev</a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#">Next → </a></li></ul></div></div></div></div>-->
                      

    <ul class="pagination pull-right">
      
         <?php 

          if($cPage > $pOffset){
echo '<li><a href="categorie.php?p=1">&laquo;</a></li>'; 
}else{
echo '<li class="disabled"><span>&laquo;</span></li>';
}
// Affichage des deux 1ères pages - pages permanentes
for($i=1;$i<=$pOffset;$i++){  
if ($i==$cPage) {   
echo '<li  class="active" ><a href="categorie.php?p='.$i.'">'.$i.'</a></li>'; 
}else {

    echo '<li><a href="categorie.php?p='.$i.'">'.$i.'</a></li>'; 

}
}
// Affichage de ... si page courante > page offset
if($cPage > $pOffset){                               
echo '<li class="disabled"><a href="#">...</a></li>'; 
}
// 
if($cPage < ($pOffset+$nbPageLimit)){
for($i=$pOffset+1;$i<=$cPage+(int)($nbPageLimit/2);$i++){        
if($i==$cPage){ 
// affichage page active
echo '<li class="active"><a href="#">'.$i.'</a></li>';
}else{
// affichage autres pages
echo '<li><a href="categorie.php?p='.$i.'">'.$i.'</a></li>'; 
}
}
}                           
if($cPage >= ($pOffset+$nbPageLimit) && $cPage < ($lastPage-$nbPageLimit)){
for($i=$cPage-(int)($nbPageLimit/2);$i<=$cPage+(int)($nbPageLimit/2);$i++){      
if($i==$cPage){ 
// affichage page active
echo '<li class="active"><a href="#">'.$i.'</a></li>';
}else{
// affichage autres pages
echo '<li><a href="categorie.php?p='.$i.'">'.$i.'</a></li>'; 
}
}
}
if($cPage >= ($lastPage-$nbPageLimit)){
for($i=$cPage-((int)($nbPageLimit/2));$i<=$lastPage-$pOffset;$i++){      
if($i==$cPage){ 
// affichage page active
echo '<li class="active"><a href="#">'.$i.'</a></li>';
}else{
// affichage autres pages
echo '<li><a href="categorie.php?p='.$i.'">'.$i.'</a></li>'; 
}
}
}
// Affichage de ... si page courante < page fin
if($cPage < ($lastPage-$pOffset)){                               
echo '<li class="disabled"><a href="#">...</a></li>'; 
}
// Affichage des deux dernières pages - pages permantentes
for($i=($lastPage-$pOffset+1);$i<=$lastPage;$i++){ 
if($i==$cPage){       
echo '<li class="active"><a href="categorie.php?p='.$i.'">'.$i.'</a></li>';
}else {
echo '<li><a href="categorie.php?p='.$i.'">'.$i.'</a></li>';
}
}
// Affichage saut à la dernière page
if($cPage < $lastPage-$pOffset){
echo '<li><a href="categorie.php?p='.$lastPage.'">&raquo;</a></li>'; 
}else{
echo '<li class="disabled"><span>&raquo;</span></li>';
}
echo '
<li class="disabled"><a href="#"><span>page <b>'.$cPage.'</b>/'.$lastPage.'</span></a></li>';


     ?>
<!--        <li><a href="#">»</a></li>-->
    </ul>
                            


                       </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">
<div class="search-row">
    <input type="text" placeholder="Search" class="form-control">
</div>
<ul class="right-side-accordion">
<li class="widget-collapsible">
    <a href="#" class="head widget-head red-bg active clearfix">
        <span class="pull-left">work progress (5)</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="prog-row side-mini-stat clearfix">
                <div class="side-graph-info">
                    <h4>Target sell</h4>
                    <p>
                        25%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="target-sell">
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="side-graph-info">
                    <h4>product delivery</h4>
                    <p>
                        55%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="p-delivery">
                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                        </div>
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="side-graph-info payment-info">
                    <h4>payment collection</h4>
                    <p>
                        25%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="p-collection">
                        <span class="pc-epie-chart" data-percent="45">
                        <span class="percent"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="side-graph-info">
                    <h4>delivery pending</h4>
                    <p>
                        44%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="d-pending">
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="col-md-12">
                    <h4>total progress</h4>
                    <p>
                        50%, Deadline 12 june 13
                    </p>
                    <div class="progress progress-xs mtop10">
                        <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                            <span class="sr-only">50% Complete</span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</li>
<li class="widget-collapsible">
    <a href="#" class="head widget-head terques-bg active clearfix">
        <span class="pull-left">contact online (5)</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Jonathan Smith</a></h4>
                    <p>
                        Work for fun
                    </p>
                </div>
                <div class="user-status text-danger">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Anjelina Joe</a></h4>
                    <p>
                        Available
                    </p>
                </div>
                <div class="user-status text-success">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Jhone Doe</a></h4>
                    <p>
                        Away from Desk
                    </p>
                </div>
                <div class="user-status text-warning">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Mark Henry</a></h4>
                    <p>
                        working
                    </p>
                </div>
                <div class="user-status text-info">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Shila Jones</a></h4>
                    <p>
                        Work for fun
                    </p>
                </div>
                <div class="user-status text-danger">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <p class="text-center">
                <a href="#" class="view-btn">View all Contacts</a>
            </p>
        </li>
    </ul>
</li>
<li class="widget-collapsible">
    <a href="#" class="head widget-head purple-bg active">
        <span class="pull-left"> recent activity (3)</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="prog-row">
                <div class="user-thumb rsn-activity">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="rsn-details ">
                    <p class="text-muted">
                        just now
                    </p>
                    <p>
                        <a href="#">John Sinna </a>Purchased new equipments for zonal office setup
                    </p>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb rsn-activity">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="rsn-details ">
                    <p class="text-muted">
                        2 min ago
                    </p>
                    <p>
                        <a href="#">Sumon </a>Purchased new equipments for zonal office setup
                    </p>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb rsn-activity">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="rsn-details ">
                    <p class="text-muted">
                        1 day ago
                    </p>
                    <p>
                        <a href="#">Mosaddek </a>Purchased new equipments for zonal office setup
                    </p>
                </div>
            </div>
        </li>
    </ul>
</li>
<li class="widget-collapsible">
    <a href="#" class="head widget-head yellow-bg active">
        <span class="pull-left"> shipment status</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="col-md-12">
                <div class="prog-row">
                    <p>
                        Full sleeve baby wear (SL: 17665)
                    </p>
                    <div class="progress progress-xs mtop10">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="prog-row">
                    <p>
                        Full sleeve baby wear (SL: 17665)
                    </p>
                    <div class="progress progress-xs mtop10">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                            <span class="sr-only">70% Completed</span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</li>
</ul>
</div>
<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<script src="js/lib/jquery.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>


<!--common script init for all pages-->
<script src="js/scripts.js"></script>

</body>
</html>

<?php }else{header("Location:login.php");} ?>
