<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monitoring Words :: by :: JAWEB.ma</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/metro.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="assets/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<?php
		session_start();
		if(isset($_SESSION['uname'])){
			$uname = $_SESSION['uname'];
			$upass = $_SESSION['upass'];
		}
		if(empty($uname) || empty($upass)){
			//echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" /> ';
			die('<meta HTTP-EQUIV="Refresh" CONTENT="0; login.php" />');
		}
		
		$sect = $_GET['section'];
		if($sect=='list' || $sect=='h_search'){ $inpage1 = 'alerte1'; }
		else{ $inpage = 'alerte'; }

	?>
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid" style="margin-top:-50px;">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<div class="slide hide">
				<i class="icon-angle-left"></i>
			</div>

<br /><br />

			<div class="clearfix"></div>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
			<!-- BEGIN SIDEBAR MENU -->
<?php
	include('config.php');
	include('sidebar.php');
?>
			<!-- END SIDEBAR MENU -->


		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						
						<!-- END STYLE CUSTOMIZER-->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<?php
							if (isset($_GET["section"])){		
								$section = htmlspecialchars($_GET["section"]);
						?>		
						<h3 class="page-title">
							<?php if($section=='search'){ echo "Planifier une alerte";} ?>
							<?php if($section=='h_search'){ echo "Planifier une alerte";} ?>
							<?php if($section=='list'){ echo "La liste";} ?>
							<?php if($section=='mod'){ echo "Alerte";} ?>
							<small>
								<?php
			                     	if($section=='search'){ echo "Planifier une alerte";}
			                     	if($section=='h_search'){ echo "Planifier une alerte";}
			                     	if($section=='list'){ echo "gérer vos alertes";}
			                     	if($section=='mod'){ echo "modifier vos alertes";}
			                    ?>
							</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.php">Tableau de bord</a> 
							</li>
							<span class="icon-angle-right"></span>
							<li>
								<a href="alerte.php?section=search">Alerte</a> 
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<?php 
					if (isset($_GET["msg"])){
						$msg = htmlspecialchars($_GET["msg"]);
					if($section == 'list' && $msg == 'update'){
									$alert = 'success';
									$msgs = 'Votre Alerte a été bien planifiée.';
								}
					if($section == 'list' && $msg == 'del'){
									$alert = 'success';
									$msgs = 'Votre Alerte a été bien supprimée.';
								}
					if($section == 'list' && $msg == 'mod'){
									$alert = 'success';
									$msgs = 'Votre Alerte a été bien modifiée.';
								}
								 ?>
									<?php
								if (isset($msg)){
							?>
								<div class="alert alert-<?php echo $alert; ?> alert-dismissable">
								  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  	<center><?php echo $msgs; ?></center>
								</div>
							<?php
								}}
							?>
						<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="portlet box blue">
							<div class="portlet-title">
		                        <h4><i class="icon-reorder"></i>
		                        	<?php
			                     		if($section=='search'){ echo "Planifier une recherche";}
			                     		if($section=='h_search'){ echo "Planifier une alerte";}
			                     		if($section=='list'){ echo "Liste des alertes";}
			                     		if($section=='mod'){ echo "Modifier l'alerte";}
			                     	?>
		                        </h4>
		                     </div>

		                     <div class="portlet-body form">
		                     	<?php
		                     		if($section=='search'){
		                     	?>
								<form class="form-horizontal" action="codes.php" method="post">
									<input name="section" value="result" type="hidden">
									<input name="alerte" value="alerteEmail" type="hidden">
			                     	<div class="control-group">
		                              <label class="control-label">Mot clé</label>
		                              <div class="controls">
		                                 <input type="text" name="mot_cle" data-original-title="Inserer votre mot clé" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>

									<div class="control-group">
		                              <label class="control-label">Site web</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="site" data-placeholder="Sélectionner un site web" class="span6 m-wrap">
		                                    <option value="tout">Tout les sites</option>
		                                 	<?php
		                                 		include('config.php');
				                           		$sitewebs = mysql_query('select * from siteweb');
				            					if(mysql_num_rows($sitewebs) == 0){
				            						echo'<option value="">Aucun site web trouvé</option>';
				            					}else{
				            						while ($s = mysql_fetch_assoc($sitewebs)) {
		            									echo '<option value="'.$s['id'].'">'.$s['name'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>
		                           	<div class="control-group">
		                              <label class="control-label">Date des données</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="qrd" data-placeholder="Sélectionner un site web" class="span6 m-wrap">
		                                    <option value="t_dates" selected="selected">Tout les dates</option>
		                                    <option value="heure">Moins d'une heure</option>
		                                    <option value="24heures">Moins de 24 heures</option>
		                                    <option value="semaine">Moins d'une semaine</option>
		                                    <option value="mois">Moins d'un mois</option>
		                                    <option value="an">Moins d'un an</option>
		                             	</select>
		                              </div>
		                           </div>
		                           <div class="control-group">
		                              <label class="control-label">Date de réception</label>
		                              <div class="controls">
		                              	<div id="dp3"  data-date-format="yyyy-mm-dd">
											  <input id="datepicker"  type="text" name="date" data-original-title="Inserer la date" data-trigger="hover" class="span6 m-wrap tooltips">                       
										</div>
		                                 
		                              </div>
		                           </div>
									<div class="form-actions">
		                              <button class="btn blue" type="submit">Planifier</button>
		                            
		                           </div>

		                        </form>  
		                     	<?php
		                     		} // End of search
		                     	?>
		                     	<?php
		                     	include('config.php');	

								$req11='SELECT * FROM `alerte` WHERE `date`=NOW()';
								$res11=mysql_query($req11); 
								$tab = mysql_fetch_assoc($res11);

								$log='SELECT email FROM `login`';
								$resLog=mysql_query($log); 
								$tabLog=mysql_fetch_assoc($resLog);

			            								?>
	<?php if($section=='h_search') {?>
				
							<form class="form-horizontal" action="codes.php" method="post">
									<input name="section" value="result" type="hidden">
									<input name="alerte_h" value="alerteEmail_h" type="hidden">
			                     	<div class="control-group">
		                              <label class="control-label">Mot clé</label>
		                              <div class="controls">
		                                 <input type="text" name="mot_cle" data-original-title="Inserer votre mot clé" data-trigger="hover" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>

									<div class="control-group">
		                              <label class="control-label">Site web</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="site" data-placeholder="Sélectionner un site web" class="span6 m-wrap">
		                                    <option value="tout">Tout les sites</option>
		                                 	<?php
		                                 		include('config.php');
				                           		$sitewebs = mysql_query('select * from siteweb');
				            					if(mysql_num_rows($sitewebs) == 0){
				            						echo'<option value="">Aucun site web trouvé</option>';
				            					}else{
				            						while ($s = mysql_fetch_assoc($sitewebs)) {
		            									echo '<option value="'.$s['id'].'">'.$s['name'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>
		                         
		                           <div class="control-group">
		                              <label class="control-label">Date début d'exécution </label>
		                              <div class="controls">
		                              	<div id="dp3"  data-date-format="yyyy-mm-dd">
											  <input id="datepicker"  type="text" name="dateD" data-original-title="Inserer la date de début" data-trigger="hover" class="span6 m-wrap tooltips">                       
										</div>
		                                 
		                              </div>
		                           </div>
		                           <div class="control-group">
		                              <label class="control-label">Date fin d'exécution</label>
		                              <div class="controls">
		                              	<div id="dp3"  data-date-format="yyyy-mm-dd">
											<input id="datepicker1"  type="text" name="dateF" data-original-title="Inserer la date de fin" data-trigger="hover" class="span6 m-wrap tooltips">                       
										</div><br/>
		                                 
		                              </div>
		                             <div class="control-group">
		                              <label class="control-label">Etat(Activer/désactiver)</label>
		                              	
									     &nbsp; &nbsp; &nbsp;<input  type="Checkbox" name="etat" value="1" data-trigger="hover" class="col-md-1">                       
									
		                           </div>
									<div class="form-actions">
		                              <button class="btn blue" type="submit">Planifier</button>
		                            
		                           </div>

		                        </form>  
	
		


						                              </div>
						                           	</div>
			  
		            								<?php
		                     			
		                     		 // End of result
		                     	?>
		                     				
		                     </div>
		  <?php }?>

		   <?php if($section=='list') {?> 

		   <table class="table table-striped table-bordered">
			            						<tr>
			            							<th style="text-align:center;">N°</th>
			            							<th style="text-align:center;">Mot clé</th>
			            							<th style="text-align:center;">Site</th>
			            							<th style="text-align:center;">Date Début</th>
			            							<th style="text-align:center;">Date Fin</th>
			            							<th style="text-align:center;">Etat</th>
			            							<th style="text-align:center;">Actions</th>
			  										
			            							
			            						</tr> 
		<?php 
include('config.php');
$req='SELECT * FROM `daily_alert`';
$res=mysql_query($req);
$count=mysql_num_rows($res);
if ($count==0) {
	echo'<th style="text-align:center;" COLSPAN="7" >Aucune alerte trouvée</th>';
}else{


while ($r=mysql_fetch_assoc($res)) {
//if we chosed the all sites
if($r['site']!="tout"){
$reqNomSite='SELECT * FROM `siteweb`WHERE id='.$r['site'];
$resNomeSite=mysql_query($reqNomSite);
$rowNomeSite=mysql_fetch_assoc($resNomeSite);	
}
 ?>		            					
												<tr>
			            							<td><?php echo $r['id']?></td>
			            							<td><?php echo $r['mot_cle']?></td>
			            							<td><?php 
			            							if($r['site']!="tout"){
			            							echo $rowNomeSite['name'];
			            						}else{
			            							echo $r['site'];
			            						}
			            							?>

			            							</td>
			            							<td><?php echo $r['dateD']?></td>
			            							<td><?php echo $r['dateF']?></td>
			            							<td style="text-align:center;">

			            							<?php 
			            								//si la date alerte est toujours different de la date now afficher le bouton mofifier
			            							if ($r['etat']!="1"){?>
			            								<div style="text-align:center;cursor: default;" class="btn">Désactivée</div>
			            							<?php } else {?>
			            							
			            							<div style="text-align:center;cursor: default;" class="btn green">Activée</div>
			            							
			            							<?php }?>
			            							
			            						</td>
			            						<td style="text-align:center;" >
			            						<a class="btn blue" href="alerte.php?section=mod&q=<?php echo $r['id']?>"><i class="icon-pencil"></i></a>
			            						<a class="btn blue"  href="resCron.php?section=liste&q=<?php echo $r['mot_cle']?>&date=<?php echo $r['dateD']?>&site=<?php echo $r['site']?>"><i class="fa fa-eye"></i></a>
				            					<a class="btn red" OnClick="return confirm('Voulez vous vraiment supprimer cette Alerte ?')" href="alerte.php?section=list&id=<?php echo $r['id']?>"><i class="icon-trash"></i></a>
			            						</td>

			            						</tr>
	<?php }}

 
		                     		if($section=='list'){
		                     			
		                     			$id = htmlspecialchars($_GET["id"]);
		                     			if (isset($section) && isset($id)){
											if($section=='list'){							
												if(mysql_query('DELETE FROM `daily_alert` WHERE  `id`='.$id)){
												//header('location : alerte.php?section=list&msg=del');
												echo'<meta HTTP-EQUIV="Refresh" CONTENT="0; alerte.php?section=list&msg=del"/> ';
											}
											}
										}
									}
		                       

	 ?>
		            							</table>
	            						


		  





		  <?php }?>
		 <?php if($section=='mod') {?> 
			
		<?php if(isset($_GET['q'])&& !empty($_GET['q'])) { 
		$q=htmlspecialchars($_GET['q']);
		$req11='SELECT * FROM `daily_alert` WHERE `id`='.$q;
		$res11=mysql_query($req11); 
		$tab = mysql_fetch_assoc($res11);
	}
// fonction selectioner un element deja enrebgistrer sur la baseb de donnees
		 function sel_option($val1, $val2){
					if ($val1 == $val2){
					return "selected";
					}else{
					return "";
					}
					}
	?>
								<form class="form-horizontal" action="codes.php" method="post">
									<input name="q" value="<?php echo $_GET['q'] ?>" type="hidden">
									<input name="alerteUpdate_h" value="alerteUpdate_h" type="hidden">
			                     	<div class="control-group">
		                              <label class="control-label">Mot clé</label>
		                              <div class="controls">
		                                 <input type="text" name="mot_cle" data-original-title="Inserer votre mot clé" data-trigger="hover" value="<?php echo $tab['mot_cle']; ?>" class="span6 m-wrap tooltips">                       
		                              </div>
		                           </div>

									<div class="control-group">
		                              <label class="control-label">Site web</label>
		                              <div class="controls">
		                                 <select tabindex="1" name="site" data-placeholder="Sélectionner un site web" class="span6 m-wrap">
		                                    <option value="tout">Tout les sites</option>
		                                 	<?php
		                                 		include('config.php');
				                           		$sitewebs = mysql_query('select * from siteweb');
				            					if(mysql_num_rows($sitewebs) == 0){
				            						echo'<option value="">Aucun site web trouvé</option>';
				            					}else{
				            						while ($s = mysql_fetch_assoc($sitewebs)) {
		            									  echo '<option value="'.$s['id'].'" '.sel_option($s['id'],$tab['site']).'>'.$s['name'].'</option>';
				            						}
				            					}
				                           ?>
		                             	</select>
		                              </div>
		                           </div>
		                           <div class="control-group">
		                              <label class="control-label">Date de début</label>
		                                <div class="controls">
		                              	<div id="dp3"  data-date-format="yyyy-mm-dd">
											<input id="datepicker"  type="text" name="dateD" data-original-title="Inserer la date de début" data-trigger="hover" value="<?php echo $tab['dateD']; ?>" class="span6 m-wrap tooltips">                       
										</div><br/>
		                                 
		                              </div>
		                           </div>
		                            <div class="control-group">
		                              <label class="control-label">Date de fin</label>
		                               <div class="controls">
		                              	<div id="dp3"  data-date-format="yyyy-mm-dd">
											<input id="datepicker1"  type="text" name="dateF" data-original-title="Inserer la date de fin" data-trigger="hover" value="<?php echo $tab['dateF']; ?>"  class="span6 m-wrap tooltips">                       
										</div><br/>
		                                 
		                              </div>
		                           </div>
		                            <div class="control-group">
		                              <label class="control-label">Activer</label>
		                              	<?php if ($tab['etat']=="1") { ?>
									     &nbsp; &nbsp; &nbsp;<input  type="Checkbox" name="etat" value="1" onclick="$(this).attr('value', this.checked ? 0 : 1)" data-trigger="hover" class="col-md-1" checked="checked">                       
									 	<?php }else { ?>
									 	 &nbsp; &nbsp; &nbsp;<input  type="Checkbox" name="etat" value="0" onclick="$(this).attr('value', this.checked ? 1 : 0)" data-trigger="hover" class="col-md-1">                       
									 	<?php } ?>
		                           </div>
									<div class="form-actions">
		                              <button class="btn blue" type="submit">Modifier</button>
		                            
		                           </div>

		                        </form>  
		        

		 <?php }?>

<?php } else{ ?>
							<meta HTTP-EQUIV="Refresh" CONTENT="0; error404.php" />
		                <?php } ?> 
		                </div>
		                   
					</div>

					
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		Application de recherche & suivi pour la Cellule AUDIO VISEUL (Préfecture Sidi Bernoussi)
		<br />
		&copy; 2014 <a href="www.jaweb.ma"> JAWEB</a>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.3.min.js"></script>			
	<script src="assets/breakpoints/breakpoints.js"></script>			
	<script src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37564768-1']);
	  _gaq.push(['_setDomainName', 'keenthemes.com']);
	  _gaq.push(['_setAllowLinker', true]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<script type="text/javascript">
	$(document).ready(function () {
                
                $('#datepicker').datepicker({
                    format: "yyyy-mm-dd"
                });  
                 $('#datepicker1').datepicker({
                    format: "yyyy-mm-dd"
                });  
            
            });
	
</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>